package mascota.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Jugador;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.JugadorRepository;
import mascota.service.JugadorService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class JugadorServiceTest {
	
	@Autowired
	private JugadorService jugadorService;
	@Autowired
	private JugadorRepository jugadorRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private EquipoRepository equipoRepository;
	
	private Long claveJugador;
	private Jugador jugador;
	
	private Long claveEquipo;
	private Equipo equipo;

	@Before
	public void setUp() throws Exception {
		claveEquipo = tHelper.generaEquipo();
		equipo = equipoRepository.read(claveEquipo);
		
		claveJugador = tHelper.generaJugadorLocal(equipo);
		jugador = jugadorRepository.read(claveJugador);
	}

	@Test
	public void testNuevoJugador() {
		Jugador jugadorNuevo = jugadorService.nuevoJugador(equipo, "Bea Calzada", 7, true);
		
		Jugador jugadorAniadido = jugadorRepository.read(jugadorNuevo.getId());
		
		assertEquals("Bea Calzada", jugadorAniadido.getNombreJugador());
		assertEquals(7, jugadorAniadido.getDorsal());
		assertEquals(true, jugadorAniadido.isCapitan());
	}
	
	@Test
	public void testModificarJugador() {
		Jugador jugadorAniadido = jugadorService.modificarJugador(jugador.getId(), jugador.getEquipo(), "Julia", jugador.getDorsal(), jugador.isCapitan());
		
		assertEquals("Julia", jugadorAniadido.getNombreJugador());
		assertEquals(14, jugadorAniadido.getDorsal());
		assertEquals(true, jugadorAniadido.isCapitan());
	}
	
	@Test
	public void testBorrarJugador() {
		List<Jugador> listaJugadores = jugadorRepository.list();
		assertEquals(1, listaJugadores.size());
		
		boolean resultado = jugadorService.borrarJugador(jugador.getId());
		
		listaJugadores = jugadorRepository.list();
		
		assertEquals(true, resultado);
		assertEquals(0, listaJugadores.size());
	}
	
	@Test
	public void testListarJugador() {
		List<Jugador> listaJugadores = jugadorService.listarJugadores();
		
		assertEquals(1, listaJugadores.size());
		
		claveJugador = tHelper.generaJugadorLocal(equipo);
		Jugador jugadorResultado = jugadorRepository.read(claveJugador);
		jugadorRepository.add(jugadorResultado);
		
		claveJugador = tHelper.generaJugadorLocal(equipo);
		jugadorResultado = jugadorRepository.read(claveJugador);
		jugadorRepository.add(jugadorResultado);
		
		listaJugadores = jugadorService.listarJugadores();
		
		assertEquals(3, listaJugadores.size());
	}
	
	@Test
	public void testBorrarJugador_Integracion() {
		List<Jugador> listaJugadores = jugadorService.listarJugadores();
		assertEquals(1, listaJugadores.size());
		
		boolean resultado = jugadorService.borrarJugador(jugador.getId());
		
		listaJugadores = jugadorService.listarJugadores();
		
		assertEquals(true, resultado);
		assertEquals(0, listaJugadores.size());
	}
}