package mascota.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Entrenador;
import mascota.helper.TestHelper;
import mascota.repository.EntrenadorRepository;
import mascota.service.EntrenadorService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EntrenadorServiceTest {
	
	@Autowired
	private EntrenadorService entrenadorService;
	@Autowired
	private EntrenadorRepository entrenadorRepository;
	@Autowired
	private TestHelper tHelper;
	
	private Long claveEntrenador;
	private Entrenador entrenador;

	@Before
	public void setUp() throws Exception {
		claveEntrenador = tHelper.generaEntrenador();
		
		entrenador = entrenadorRepository.read(claveEntrenador);
	}

	@Test
	public void testNuevoEntrenador() {
		Entrenador entrenadorNuevo = entrenadorService.nuevoEntrenador("David");
		
		Entrenador entrenadorAniadido = entrenadorRepository.read(entrenadorNuevo.getId());
		
		assertEquals("David", entrenadorAniadido.getNombreEntrenador());
	}
	
	@Test
	public void testModificarEntrenador() {
		Entrenador entrenadorNuevo = entrenadorService.modificarEntrenador(entrenador.getId(), "Javier");
		
		Entrenador entrenadorAniadido = entrenadorRepository.read(entrenadorNuevo.getId());
		
		assertEquals("Javier", entrenadorAniadido.getNombreEntrenador());
	}
	
	@Test
	public void testBorrarEntrenador() {
		List<Entrenador> listaEntrenadores = entrenadorRepository.list();
		assertEquals(1, listaEntrenadores.size());
		
		boolean resultado = entrenadorService.borrarEntrenador(entrenador.getId());
		
		listaEntrenadores = entrenadorRepository.list();
		
		assertEquals(true, resultado);
		assertEquals(0, listaEntrenadores.size());
	}
	
	@Test
	public void testListarEntrenadores() {
		List<Entrenador> listaEntrenadores = entrenadorService.listarEntrenadores();
		
		assertEquals(1, listaEntrenadores.size());
		
		claveEntrenador = tHelper.generaEntrenador();
		Entrenador entrenadorResultado = entrenadorRepository.read(claveEntrenador);
		entrenadorRepository.add(entrenadorResultado);
		
		claveEntrenador = tHelper.generaEntrenador();
		entrenadorResultado = entrenadorRepository.read(claveEntrenador);
		entrenadorRepository.add(entrenadorResultado);
		
		listaEntrenadores = entrenadorService.listarEntrenadores();
		
		assertEquals(3, listaEntrenadores.size());
	}
	
	@Test
	public void testBorrarEntrenador_Integracion() {
		List<Entrenador> listaEntrenadores = entrenadorService.listarEntrenadores();
		assertEquals(1, listaEntrenadores.size());
		
		boolean resultado = entrenadorService.borrarEntrenador(entrenador.getId());
		
		listaEntrenadores = entrenadorService.listarEntrenadores();
		
		assertEquals(true, resultado);
		assertEquals(0, listaEntrenadores.size());
	}
}