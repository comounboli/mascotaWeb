package mascota.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Estadistica;
import mascota.dominio.Jugador;
import mascota.dominio.Partido;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.EstadisticaRepository;
import mascota.repository.JugadorRepository;
import mascota.repository.PartidoRepository;
import mascota.service.EstadisticaService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EstadisticaServiceTest {
	
	@Autowired
	private EstadisticaService estadisticaService;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private JugadorRepository jugadorRepository;
	@Autowired
	private EstadisticaRepository estadisticaRepository;
	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private PartidoRepository partidoRepository;
	
	private static final double DELTA = 0.001;

	private Jugador jugadorLocal;
	private Long claveJugadorLocal;
	private Jugador jugadorVisitante;
	private Long claveJugadorVisitante;
	
	private Long claveEquipoLocal;
	private Equipo equipoLocal;
	private Long claveEquipoVisitante;
	private Equipo equipoVisitante;
	
	private Long clavePartido;
	private Partido partido;
	
	private Long claveEstadisticaLocal;
	private Estadistica estadisticaLocal;
	private Long claveEstadisticaVisitante;
	private Estadistica estadisticaVisitante;

	@Before
	public void setUp() throws Exception {
		claveEquipoLocal = tHelper.generaEquipo();
		equipoLocal = equipoRepository.read(claveEquipoLocal);
		claveEquipoVisitante = tHelper.generaEquipo();
		equipoVisitante = equipoRepository.read(claveEquipoVisitante);

		clavePartido = tHelper.generaPartido(equipoLocal, equipoVisitante);
		partido = partidoRepository.read(clavePartido);
		
		claveJugadorLocal = tHelper.generaJugadorLocal(equipoLocal);
		jugadorLocal = jugadorRepository.read(claveJugadorLocal);
		claveJugadorVisitante = tHelper.generaJugadorVisitante(equipoVisitante);
		jugadorVisitante = jugadorRepository.read(claveJugadorVisitante);
		
		claveEstadisticaLocal = tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);
		estadisticaLocal = estadisticaRepository.read(claveEstadisticaLocal);
		claveEstadisticaVisitante = tHelper.generaEstadisticaVisitante(equipoLocal, equipoVisitante);
		estadisticaVisitante = estadisticaRepository.read(claveEstadisticaVisitante);
	}
	
	@Test
	public void testSumarTiroLibres_Local_ComprobacionEnPartido() {
		estadisticaService.sumarLibreAnotado(estadisticaLocal);
		estadisticaService.sumarLibreAnotado(estadisticaLocal);
		estadisticaService.sumarLibreFallado(estadisticaLocal);
		estadisticaLocal = estadisticaService.sumarLibreAnotado(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(3, estadisticaActual.getTirosLibresAnotados());
		assertEquals(75, estadisticaActual.getTirosLibresPorcentaje(), DELTA);
		
		assertEquals(3, estadisticaActual.getPartido().getPuntosLocal());
	}
	
	@Test
	public void testSumarTiroLibres_Visitante_ComprobacionEnPartido() {
		estadisticaService.sumarLibreAnotado(estadisticaVisitante);
		estadisticaService.sumarLibreAnotado(estadisticaVisitante);
		estadisticaService.sumarLibreFallado(estadisticaVisitante);
		estadisticaVisitante = estadisticaService.sumarLibreAnotado(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(3, estadisticaActual.getTirosLibresAnotados());
		assertEquals(75, estadisticaActual.getTirosLibresPorcentaje(), DELTA);
		
		assertEquals(3, estadisticaActual.getPartido().getPuntosVisitante());
	}
	
	@Test
	public void testIntegracion_Marcador() {
		estadisticaService.sumarDosAnotado(estadisticaLocal);
		estadisticaService.sumarLibreAnotado(estadisticaLocal);
		estadisticaService.sumarDosFallado(estadisticaLocal);
		estadisticaService.sumarLibreFallado(estadisticaLocal);
		estadisticaService.sumarLibreFallado(estadisticaLocal);
		estadisticaService.sumarTripleFallado(estadisticaLocal);
		estadisticaService.sumarTripleAnotado(estadisticaLocal);
		estadisticaLocal = estadisticaService.sumarLibreAnotado(estadisticaLocal);
		estadisticaService.sumarLibreAnotado(estadisticaVisitante);
		estadisticaService.sumarLibreAnotado(estadisticaVisitante);
		estadisticaService.sumarLibreFallado(estadisticaVisitante);
		estadisticaVisitante = estadisticaService.sumarLibreAnotado(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(2, estadisticaActual.getTirosLibresAnotados());
		assertEquals(2, estadisticaActual.getTirosTriples());
		assertEquals(1, estadisticaActual.getTirosTriplesAnotados());
		assertEquals(2, estadisticaActual.getTirosDos());
		assertEquals(1, estadisticaActual.getTirosDosAnotados());
		assertEquals(7, estadisticaActual.getPartido().getPuntosLocal());
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(3, estadisticaActual.getTirosLibresAnotados());
		assertEquals(3, estadisticaActual.getPartido().getPuntosVisitante());
	}
	
	@Test
	public void testSumarFaltaCometida_Local() {
		estadisticaLocal = estadisticaService.sumarFaltaCometida(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(1, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante() {
		estadisticaVisitante = estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(1, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Local_ComprobacionConPartido() {
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaLocal = estadisticaService.sumarFaltaCometida(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(3, estadisticaActual.getPartido().getFcLocal());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante_ComprobacionConPartido() {
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaVisitante = estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(2, estadisticaActual.getPartido().getFcVisitante());
	}
	
	@Test
	public void testSumarFaltaCometida_Local_ComprobacionConPartido_BONUS_JugadorExpulsado() {
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaLocal = estadisticaService.sumarFaltaCometida(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(4, estadisticaActual.getPartido().getFcLocal());
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante_ComprobacionConPartido_BONUS_JugadorExpulsado() {
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaVisitante = estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(4, estadisticaActual.getPartido().getFcVisitante());
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Local_JugadorCincoFaltas(){
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaLocal = estadisticaService.sumarFaltaCometida(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante_JugadorCincoFaltas(){
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		estadisticaLocal = estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaRecibida_Local() {
		estadisticaLocal = estadisticaService.sumarFaltaRecibida(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(3, estadisticaActual.getFaltasRecibidas());
	}
	
	@Test
	public void testSumarFaltaRecibida_Visitante() {
		estadisticaVisitante = estadisticaService.sumarFaltaRecibida(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(3, estadisticaActual.getFaltasRecibidas());
	}
	
	@Test
	public void testObtenerValoracion_Local() {
		int resultado = estadisticaService.obtenerValoracion(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
	}
	
	@Test
	public void testObtenerValoracion_Visitante() {
		int resultado = estadisticaService.obtenerValoracion(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
	}
	
	@Test
	public void testObtenerValoracion_Local_Integracion() {
		int resultado  = estadisticaService.obtenerValoracion(estadisticaLocal);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaService.sumarFaltaRecibida(estadisticaLocal);
		estadisticaService.sumarFaltaRecibida(estadisticaLocal);
		resultado = estadisticaService.obtenerValoracion(estadisticaLocal);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(4, estadisticaActual.getValoracion());
		assertEquals(4, resultado);
		
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		estadisticaService.sumarFaltaCometida(estadisticaLocal);
		resultado = estadisticaService.obtenerValoracion(estadisticaLocal);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(1, estadisticaActual.getValoracion());
		assertEquals(1, resultado);
		
		estadisticaService.sumarTripleAnotado(estadisticaLocal);
		estadisticaService.sumarTripleFallado(estadisticaLocal);
		estadisticaService.sumarTripleFallado(estadisticaLocal);
		resultado = estadisticaService.obtenerValoracion(estadisticaLocal);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaService.sumarLibreAnotado(estadisticaLocal);
		estadisticaService.sumarLibreFallado(estadisticaLocal);
		estadisticaService.sumarLibreFallado(estadisticaLocal);
		estadisticaService.sumarLibreFallado(estadisticaLocal);
		resultado = estadisticaService.obtenerValoracion(estadisticaLocal);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocal.getId());
		
		assertEquals(0, estadisticaActual.getValoracion());
		assertEquals(0, resultado);
	}
	
	@Test
	public void testObtenerValoracion_Visitante_Integracion() {
		int resultado  = estadisticaService.obtenerValoracion(estadisticaVisitante);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaService.sumarFaltaRecibida(estadisticaVisitante);
		resultado = estadisticaService.obtenerValoracion(estadisticaVisitante);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(3, estadisticaActual.getValoracion());
		assertEquals(3, resultado);
		
		estadisticaService.sumarFaltaCometida(estadisticaVisitante);
		resultado = estadisticaService.obtenerValoracion(estadisticaVisitante);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaService.sumarTripleAnotado(estadisticaVisitante);
		estadisticaService.sumarTripleAnotado(estadisticaVisitante);
		resultado = estadisticaService.obtenerValoracion(estadisticaVisitante);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(8, estadisticaActual.getValoracion());
		assertEquals(8, resultado);
		
		estadisticaService.sumarLibreFallado(estadisticaVisitante);
		resultado = estadisticaService.obtenerValoracion(estadisticaVisitante);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitante.getId());
		
		assertEquals(7, estadisticaActual.getValoracion());
		assertEquals(7, resultado);
	}
	
	@Test
	public void testListarEstadisticas() {
		Collection<Estadistica> listaEstadisticas = estadisticaService.listarEstadisticas();
		
		assertEquals(2, listaEstadisticas.size());
		
		claveEstadisticaLocal = tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);
		estadisticaLocal = estadisticaRepository.read(claveEstadisticaLocal);
		estadisticaRepository.add(estadisticaLocal);
		
		claveEstadisticaVisitante = tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);
		estadisticaVisitante = estadisticaRepository.read(claveEstadisticaVisitante);
		estadisticaRepository.add(estadisticaVisitante);
		
		listaEstadisticas = estadisticaService.listarEstadisticas();
		
		assertEquals(4, listaEstadisticas.size());
	}
	
	@Test
	public void testNuevaEstadistica_Local() {
		Estadistica estadisticaNueva = estadisticaService.nuevaEstadisticaEnBlanco(partido.getId(), jugadorLocal.getId());
		
		Estadistica estadisticaAniadida = estadisticaRepository.read(estadisticaNueva.getId());
		
		assertNotNull(estadisticaAniadida.getJugador().getEquipo());
	}
	
	@Test
	public void testNuevaEstadistica_Visitante() {
		Estadistica estadisticaNueva = estadisticaService.nuevaEstadisticaEnBlanco(partido.getId(), jugadorVisitante.getId());
		
		Estadistica estadisticaAniadida = estadisticaRepository.read(estadisticaNueva.getId());
		
		assertNotNull(estadisticaAniadida.getJugador().getEquipo());
	}
}