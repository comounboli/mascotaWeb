package mascota.service;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Entrenador;
import mascota.dominio.Equipo;
import mascota.helper.TestHelper;
import mascota.repository.EntrenadorRepository;
import mascota.repository.EquipoRepository;
import mascota.service.EquipoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EquipoServiceTest {
	
	@Autowired
	private EquipoService equipoService;
	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private EntrenadorRepository entrenadorRepository;
	
	private Long claveEquipo;
	private Equipo equipo;
	
	private Long claveEntrenador;
	private Entrenador entrenador;

	@Before
	public void setUp() throws Exception {
		claveEntrenador = tHelper.generaEntrenador();
		entrenador = entrenadorRepository.read(claveEntrenador);
		
		claveEquipo = tHelper.generaEquipo();
		equipo = equipoRepository.read(claveEquipo);
	}

	@Test
	public void testNuevoEquipo() {
		Equipo equipoNuevo = equipoService.nuevoEquipo(entrenador, "CBT");
		
		Equipo equipoAniadido = equipoRepository.read(equipoNuevo.getId());
		
		assertEquals("CBT", equipoAniadido.getNombreEquipo());
	}

	@Test
	public void testModificarEquipo() {
		Equipo equipoAniadido = equipoService.modificarEquipo(equipo.getId(), equipo.getEntrenador(), "Cantbasket");

		assertEquals("Cantbasket", equipoAniadido.getNombreEquipo());
	}

	@Test
	public void testBorrarEquipo() {
		List<Equipo> listaEquipos = equipoRepository.list();
		assertEquals(1, listaEquipos.size());
		
		boolean resultado = equipoService.borrarEquipo(equipo.getId());
		
		listaEquipos = equipoRepository.list();
		
		assertEquals(true, resultado);
		assertEquals(0, listaEquipos.size());
	}
	
	@Test
	public void testListarEquipo() {
		List<Equipo> listaEquipos = equipoService.listarEquipos();
		
		assertEquals(1, listaEquipos.size());
		
		claveEquipo = tHelper.generaEquipo();
		Equipo equipoResultado = equipoRepository.read(claveEquipo);
		equipoRepository.add(equipoResultado);
		
		claveEquipo = tHelper.generaEquipo();
		equipoResultado = equipoRepository.read(claveEquipo);
		equipoRepository.add(equipoResultado);
		
		listaEquipos = equipoService.listarEquipos();
		
		assertEquals(3, listaEquipos.size());
	}
	
	@Test
	public void testBorrarEquipo_Integracion() {
		List<Equipo> listaEquipos = equipoService.listarEquipos();
		assertEquals(1, listaEquipos.size());
		
		boolean resultado = equipoService.borrarEquipo(equipo.getId());
		
		listaEquipos = equipoService.listarEquipos();
		
		assertEquals(true, resultado);
		assertEquals(0, listaEquipos.size());
	}
}