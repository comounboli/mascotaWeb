package mascota.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Partido;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.PartidoRepository;
import mascota.service.PartidoService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class PartidoServiceTest {

	@Autowired
	private PartidoService partidoService;
	@Autowired
	private PartidoRepository partidoRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private EquipoRepository equipoRepository;
	
	private Long clavePartido;
	private Partido partido;
	
	private Long claveEquipoLocal;
	private Equipo equipoLocal;
	private Long claveEquipoVisitante;
	private Equipo equipoVisitante;

	@Before
	public void setUp() throws Exception {
		claveEquipoLocal = tHelper.generaEquipo();
		equipoLocal = equipoRepository.read(claveEquipoLocal);
		claveEquipoVisitante = tHelper.generaEquipo();
		equipoVisitante = equipoRepository.read(claveEquipoVisitante);
		
		clavePartido = tHelper.generaPartido(equipoLocal, equipoVisitante);
		partido = partidoRepository.read(clavePartido);
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Local() {
		boolean resultado = partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoLocal().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(true, resultado);
		assertEquals(1, partidoModificado.getFtLocal());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Visitante() {
		boolean resultado = partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoVisitante().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(true, resultado);
		assertEquals(1, partidoModificado.getFtVisitante());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Local_ACero() {
		partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoLocal().getId());
		boolean resultado = partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoLocal().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getFtLocal());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Visitante_ACero() {
		partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoVisitante().getId());
		boolean resultado = partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoVisitante().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getFtVisitante());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Local_YaExpulsado() {
		partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoLocal().getId());
		partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoLocal().getId());
		boolean resultado = partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoLocal().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(false, resultado);
		assertEquals(2, partidoModificado.getFtLocal());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Visitante_YaExpulsado() {
		partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoVisitante().getId());
		partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoVisitante().getId());
		boolean resultado = partidoService.sumarFaltaTecnicaEntrenador(partido, partido.getEquipoVisitante().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(false, resultado);
		assertEquals(2, partidoModificado.getFtVisitante());
	}
	
	@Test
	public void testPedirTM_Local() {
		boolean resultado = partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getTmLocal());
	}
	
	@Test
	public void testPedirTM_Visitante() {
		boolean resultado = partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getTmVisitante());
	}
	
	@Test
	public void testPedirTM_Local_ACero() {
		partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		boolean resultado = partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(0, partidoModificado.getTmLocal());
		assertEquals(true, resultado);
	} 
	
	@Test
	public void testPedirTM_Visitante_ACero() {
		partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		boolean resultado = partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(0, partidoModificado.getTmVisitante());
		assertEquals(true, resultado);
	} 
	
	@Test
	public void testPedirTM_YaConsumidos_Local() {
		partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		boolean resultado = partidoService.pedirTM(partido, partido.getEquipoLocal().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(0, partidoModificado.getTmLocal());
		assertEquals(false, resultado);
	} 
	
	@Test
	public void testPedirTM_YaConsumidos_Visitante() {
		partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		boolean resultado = partidoService.pedirTM(partido, partido.getEquipoVisitante().getId());
		
		Partido partidoModificado = partidoRepository.read(partido.getId());
		
		assertEquals(0, partidoModificado.getTmVisitante());
		assertEquals(false, resultado);
	} 

	@Test
	public void testNuevoPartido() {
		Partido partidoNuevo = partidoService.nuevoPartidoEnBlanco(partido.getEquipoLocal().getId(), partido.getEquipoVisitante().getId());
		
		Partido partidoAniadido = partidoRepository.read(partidoNuevo.getId());
		
		assertEquals(0, partidoAniadido.getPuntosLocal());
	}
	
	@Test
	public void testModificarPartido() {
		Partido partidoAniadido = partidoService.modificarPartido(partido.getId(), partido.getEquipoLocal().getId(), partido.getEquipoVisitante().getId());

		assertNotNull(partidoAniadido.getEquipoVisitante());
	}

	@Test
	public void testBorrarPartido_Integracion() {
		Collection<Partido> listaPartidos = partidoService.listarPartidos();
		assertEquals(1, listaPartidos.size());
		
		boolean resultado = partidoService.borrarPartido(partido);
		
		listaPartidos = partidoService.listarPartidos();
		
		assertEquals(true, resultado);
		assertEquals(0, listaPartidos.size());
	}
	
	@Test
	public void testCambiarPosesion() {
		assertEquals(true, partido.isPosesionLocal());
		
		Partido partidoAniadido = partidoService.cambiarPosesion(partido);
		
		assertEquals(false, partidoAniadido.isPosesionLocal());
		
		partidoAniadido = partidoService.cambiarPosesion(partido);
		
		assertEquals(true, partidoAniadido.isPosesionLocal());
	}
}