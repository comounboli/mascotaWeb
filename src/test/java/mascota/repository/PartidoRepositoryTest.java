package mascota.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Partido;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.PartidoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class PartidoRepositoryTest {
	
	@Autowired
	private PartidoRepository partidoRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private EquipoRepository equipoRepository;
	
	@PersistenceContext
	private EntityManager em;
	
	private Long claveEquipoLocal;
	private Equipo equipoLocal;
	private Long claveEquipoVisitante;
	private Equipo equipoVisitante;
	
	@Before
	public void setUp() throws Exception {
		claveEquipoLocal = tHelper.generaEquipo();
		equipoLocal = equipoRepository.read(claveEquipoLocal);
		claveEquipoVisitante = tHelper.generaEquipo();
		equipoVisitante = equipoRepository.read(claveEquipoVisitante);
	}

	@Test
	public void testAdd() {
		Partido partido = new Partido();
		partido.setTmLocal(5);
		partido.setTmVisitante(5);
		assertNull(partido.getId());

		partidoRepository.add(partido);

		assertNotNull(partido.getTmVisitante());
	}

	@Test
	public void testRead() {
		Long clavePrimaria = tHelper.generaPartido(equipoLocal, equipoVisitante);

		Partido resultado = partidoRepository.read(clavePrimaria);

		assertEquals(0, resultado.getFtVisitante());
	}

	@Test(expected=PersistenceException.class)
	public void testRead_noExiste() {
		Long clavePrimaria = Long.MIN_VALUE;

		Partido resultado = partidoRepository.read(clavePrimaria);

		assertEquals("Pablo", resultado.getFtVisitante());
	}

	@Test
	public void testList() {
		tHelper.generaPartido(equipoLocal, equipoVisitante);
		tHelper.generaPartido(equipoLocal, equipoVisitante);
		tHelper.generaPartido(equipoLocal, equipoVisitante);

		List<Partido> resultado = partidoRepository.list();

		assertTrue(resultado.size()>= 3);
	}

	@Test
	public void testDelete() {
		Long clavePrimaria = tHelper.generaPartido(equipoLocal, equipoVisitante);

		partidoRepository.delete(clavePrimaria);
		Partido p;
		try {
			p  = em.find(Partido.class, clavePrimaria);
		} catch (PersistenceException pe){
			return;
		}
		assertNull(p);
	}

	@Test
	public void testUpdate() {
		Long clavePrimaria = tHelper.generaPartido(equipoLocal, equipoVisitante);

		Partido partido2 = new Partido();
		partido2.setId(clavePrimaria);
		partido2.setFtVisitante(2);

		Partido resultado = partidoRepository.update(partido2);

		Partido enBBDD = em.find(Partido.class, clavePrimaria);
		assertEquals(2, enBBDD.getFtVisitante());
		assertEquals(2, resultado.getFtVisitante());
	}
}