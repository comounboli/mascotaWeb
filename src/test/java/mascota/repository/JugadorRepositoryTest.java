package mascota.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Jugador;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.JugadorRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class JugadorRepositoryTest {

	@Autowired
	private JugadorRepository jugadorRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private EquipoRepository equipoRepository;;

	@PersistenceContext
	private EntityManager em;
	
	private Long claveEquipo;
	private Equipo equipo;

	
	@Before
	public void setUp() throws Exception {
		claveEquipo = tHelper.generaEquipo();
		equipo = equipoRepository.read(claveEquipo);
	}

	@Test
	public void testAdd() {
		Jugador jugador = new Jugador();
		jugador.setNombreJugador("Cote");
		assertNull(jugador.getId());

		jugadorRepository.add(jugador);

		assertNotNull(jugador.getNombreJugador());
	}

	@Test
	public void testRead() {
		Long clavePrimaria = tHelper.generaJugadorLocal(equipo);

		Jugador resultado = jugadorRepository.read(clavePrimaria);

		assertEquals("Vic", resultado.getNombreJugador());
	}

	@Test(expected=PersistenceException.class)
	public void testRead_noExiste() {
		Long clavePrimaria = Long.MIN_VALUE;

		Jugador resultado = jugadorRepository.read(clavePrimaria);

		assertEquals("Pablo", resultado.getNombreJugador());
	}

	@Test
	public void testList() {
		tHelper.generaJugadorLocal(equipo);
		tHelper.generaJugadorLocal(equipo);
		tHelper.generaJugadorLocal(equipo);

		List<Jugador> resultado = jugadorRepository.list();

		assertTrue(resultado.size()>= 3);
	}

	@Test
	public void testDelete() {
		Long clavePrimaria = tHelper.generaJugadorLocal(equipo);

		jugadorRepository.delete(clavePrimaria);
		Jugador j;
		try {
			j  = em.find(Jugador.class, clavePrimaria);
		} catch (PersistenceException pe){
			return;
		}
		assertNull(j);
	}

	@Test
	public void testUpdate() {
		Long clavePrimaria = tHelper.generaJugadorLocal(equipo);

		Jugador jugador2 = new Jugador();
		jugador2.setId(clavePrimaria);
		jugador2.setNombreJugador("Raúl");

		Jugador resultado = jugadorRepository.update(jugador2);

		Jugador enBBDD = em.find(Jugador.class, clavePrimaria);
		assertEquals("Raúl", enBBDD.getNombreJugador());
		assertEquals("Raúl", resultado.getNombreJugador());
	}
}