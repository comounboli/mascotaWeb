package mascota.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Entrenador;
import mascota.helper.TestHelper;
import mascota.repository.EntrenadorRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EntrenadorRepositoryTest {

	@Autowired
	private EntrenadorRepository entrenadorRepository;
	@Autowired
	private TestHelper tHelper;

	@PersistenceContext
	private EntityManager em;

	@Test
	public void testAdd() {
		Entrenador entrenador = new Entrenador();
		entrenador.setNombreEntrenador("Gonzalo");
		assertNull(entrenador.getId());

		entrenadorRepository.add(entrenador);

		assertNotNull(entrenador.getNombreEntrenador());
	}

	@Test
	public void testRead() {
		Long clavePrimaria = tHelper.generaEntrenador();

		Entrenador resultado = entrenadorRepository.read(clavePrimaria);

		assertEquals("Gonchy", resultado.getNombreEntrenador());
	}

	@Test(expected=PersistenceException.class)
	public void testRead_noExiste() {
		Long clavePrimaria = Long.MIN_VALUE;

		Entrenador resultado = entrenadorRepository.read(clavePrimaria);

		assertEquals("Pablo", resultado.getNombreEntrenador());
	}

	@Test
	public void testList() {
		tHelper.generaEntrenador();
		tHelper.generaEntrenador();
		tHelper.generaEntrenador();

		List<Entrenador> resultado = entrenadorRepository.list();

		assertTrue(resultado.size()>= 3);
	}

	@Test
	public void testDelete() {
		Long clavePrimaria = tHelper.generaEntrenador();

		entrenadorRepository.delete(clavePrimaria);
		Entrenador en;
		try {
			en  = em.find(Entrenador.class, clavePrimaria);
		} catch (PersistenceException pe){
			return;
		}
		assertNull(en);
	}

	@Test
	public void testUpdate() {
		Long clavePrimaria = tHelper.generaEntrenador();

		Entrenador entrenador2 = new Entrenador();
		entrenador2.setId(clavePrimaria);
		entrenador2.setNombreEntrenador("David");

		Entrenador resultado = entrenadorRepository.update(entrenador2);

		Entrenador enBBDD = em.find(Entrenador.class, clavePrimaria);
		assertEquals("David", enBBDD.getNombreEntrenador());
		assertEquals("David", resultado.getNombreEntrenador());
	}
}