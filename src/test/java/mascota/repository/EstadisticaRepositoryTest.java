package mascota.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Estadistica;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.EstadisticaRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EstadisticaRepositoryTest {

	@Autowired
	private EstadisticaRepository estadisticaRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private EquipoRepository equipoRepository;

	@PersistenceContext
	private EntityManager em;
	
	private Long claveEquipoLocal;
	private Equipo equipoLocal;
	private Long claveEquipoVisitante;
	private Equipo equipoVisitante;
	
	@Before
	public void setUp() throws Exception {
		claveEquipoLocal = tHelper.generaEquipo();
		equipoLocal = equipoRepository.read(claveEquipoLocal);
		claveEquipoVisitante = tHelper.generaEquipo();
		equipoVisitante = equipoRepository.read(claveEquipoVisitante);
	}

	@Test
	public void testAdd() {
		Estadistica estadistica = new Estadistica();
		estadistica.setFaltasCometidas(4);;
		assertNull(estadistica.getId());

		estadisticaRepository.add(estadistica);

		assertNotNull(estadistica.getFaltasCometidas());
	}

	@Test
	public void testRead() {
		Long clavePrimaria = tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);

		Estadistica resultado = estadisticaRepository.read(clavePrimaria);

		assertEquals(2, resultado.getFaltasRecibidas());
	}

	@Test(expected=PersistenceException.class)
	public void testRead_noExiste() {
		Long clavePrimaria = Long.MIN_VALUE;

		Estadistica resultado = estadisticaRepository.read(clavePrimaria);

		assertEquals(3, resultado.getFaltasRecibidas());
	}

	@Test
	public void testList() {
		tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);
		tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);
		tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);

		List<Estadistica> resultado = estadisticaRepository.list();

		assertTrue(resultado.size()>= 3);
	}

	@Test
	public void testDelete() {
		Long clavePrimaria = tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);

		estadisticaRepository.delete(clavePrimaria);
		Estadistica es;
		try {
			es  = em.find(Estadistica.class, clavePrimaria);
		} catch (PersistenceException pe){
			return;
		}
		assertNull(es);
	}

	@Test
	public void testUpdate() {
		Long clavePrimaria = tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);

		Estadistica equipo2 = new Estadistica();
		equipo2.setId(clavePrimaria);
		equipo2.setTirosDos(10);

		Estadistica resultado = estadisticaRepository.update(equipo2);

		Estadistica enBBDD = em.find(Estadistica.class, clavePrimaria);
		assertEquals(10, enBBDD.getTirosDos());
		assertEquals(10, resultado.getTirosDos());
	}
}