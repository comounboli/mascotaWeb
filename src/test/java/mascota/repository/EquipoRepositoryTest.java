package mascota.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EquipoRepositoryTest {

	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private TestHelper tHelper;

	@PersistenceContext
	private EntityManager em;

	@Test
	public void testAdd() {
		Equipo equipo = new Equipo();
		equipo.setNombreEquipo("CBT");
		assertNull(equipo.getId());

		equipoRepository.add(equipo);

		assertNotNull(equipo.getNombreEquipo());
	}

	@Test
	public void testRead() {
		Long clavePrimaria = tHelper.generaEquipo();

		Equipo resultado = equipoRepository.read(clavePrimaria);

		assertEquals("Daygon", resultado.getNombreEquipo());
	}

	@Test(expected=PersistenceException.class)
	public void testRead_noExiste() {
		Long clavePrimaria = Long.MIN_VALUE;

		Equipo resultado = equipoRepository.read(clavePrimaria);

		assertEquals("Pablo", resultado.getNombreEquipo());
	}

	@Test
	public void testList() {
		tHelper.generaEquipo();
		tHelper.generaEquipo();
		tHelper.generaEquipo();

		List<Equipo> resultado = equipoRepository.list();

		assertTrue(resultado.size()>= 3);
	}

	@Test
	public void testDelete() {
		Long clavePrimaria = tHelper.generaEquipo();

		equipoRepository.delete(clavePrimaria);
		Equipo eq;
		try {
			eq  = em.find(Equipo.class, clavePrimaria);
		} catch (PersistenceException pe){
			return;
		}
		assertNull(eq);
	}

	@Test
	public void testUpdate() {
		Long clavePrimaria = tHelper.generaEquipo();

		Equipo equipo2 = new Equipo();
		equipo2.setId(clavePrimaria);
		equipo2.setNombreEquipo("Tirso");

		Equipo resultado = equipoRepository.update(equipo2);

		Equipo enBBDD = em.find(Equipo.class, clavePrimaria);
		assertEquals("Tirso", enBBDD.getNombreEquipo());
		assertEquals("Tirso", resultado.getNombreEquipo());
	}
}