package mascota.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.controller.EstadisticaController;
import mascota.dominio.ConverterEquipoImpl;
import mascota.dominio.ConverterEstadisticaImpl;
import mascota.dominio.ConverterJugadorImpl;
import mascota.dominio.ConverterPartidoImpl;
import mascota.dominio.Equipo;
import mascota.dominio.EquipoDTO;
import mascota.dominio.Estadistica;
import mascota.dominio.EstadisticaDTO;
import mascota.dominio.Jugador;
import mascota.dominio.JugadorDTO;
import mascota.dominio.Partido;
import mascota.dominio.PartidoDTO;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.EstadisticaRepository;
import mascota.repository.JugadorRepository;
import mascota.repository.PartidoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EstadisticaControllerTest {
	
	@Autowired
	private EstadisticaController estadisticaController;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private JugadorRepository jugadorRepository;
	@Autowired
	private EstadisticaRepository estadisticaRepository;
	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private PartidoRepository partidoRepository;
	@Autowired
	private ConverterPartidoImpl converterPartido;
	@Autowired
	private ConverterEquipoImpl converterEquipo;
	@Autowired
	private ConverterJugadorImpl converterJugador;
	@Autowired
	private ConverterEstadisticaImpl converterEstadistica;

	
	private static final double DELTA = 0.001;
	
	private Long clavePartido;
	private Partido partido;
	private PartidoDTO partidoDTO;
	private Long claveEquipoLocal;
	private Equipo equipoLocal;
	private EquipoDTO equipoLocalDTO;
	private Long claveEquipoVisitante;
	private Equipo equipoVisitante;
	private EquipoDTO equipoVisitanteDTO;
	private Long claveJugadorLocal;
	private Jugador jugadorLocal;
	private JugadorDTO jugadorLocalDTO;
	private Long claveJugadorVisitante;
	private Jugador jugadorVisitante;
	private JugadorDTO jugadorVisitanteDTO;
	
	private Long claveEstadisticaLocal;
	private Estadistica estadisticaLocal;
	private EstadisticaDTO estadisticaLocalDTO;
	private Long claveEstadisticaVisitante;
	private Estadistica estadisticaVisitante;
	private EstadisticaDTO estadisticaVisitanteDTO;

	@Before
	public void setUp() throws Exception {
		claveEquipoLocal = tHelper.generaEquipo();
		equipoLocal = equipoRepository.read(claveEquipoLocal);
		equipoLocalDTO = converterEquipo.converToDTO(equipoLocal);
		claveEquipoVisitante = tHelper.generaEquipo();
		equipoVisitante = equipoRepository.read(claveEquipoVisitante);
		equipoVisitanteDTO = converterEquipo.converToDTO(equipoVisitante);
		clavePartido = tHelper.generaPartido(equipoLocal, equipoVisitante);
		partido = partidoRepository.read(clavePartido);
		partidoDTO = converterPartido.converToDTO(partido);
		
		claveJugadorLocal = tHelper.generaJugadorLocal(equipoLocal);
		jugadorLocal = jugadorRepository.read(claveJugadorLocal);
		jugadorLocalDTO = converterJugador.converToDTO(jugadorLocal);
		claveJugadorVisitante = tHelper.generaJugadorVisitante(equipoVisitante);
		jugadorVisitante = jugadorRepository.read(claveJugadorVisitante);
		jugadorVisitanteDTO = converterJugador.converToDTO(jugadorVisitante);
		
		claveEstadisticaLocal = tHelper.generaEstadisticaLocal(equipoLocal, equipoVisitante);
		estadisticaLocal = estadisticaRepository.read(claveEstadisticaLocal);
		estadisticaLocalDTO = converterEstadistica.converToDTO(estadisticaLocal);
		claveEstadisticaVisitante = tHelper.generaEstadisticaVisitante(equipoLocal, equipoVisitante);
		estadisticaVisitante = estadisticaRepository.read(claveEstadisticaVisitante);
		estadisticaVisitanteDTO = converterEstadistica.converToDTO(estadisticaVisitante);
	}
	
	@Test
	public void testSumarTiroLibres_Local_ComprobacionEnPartido() {
		estadisticaController.sumarLibreAnotado(estadisticaLocalDTO);
		estadisticaController.sumarLibreAnotado(estadisticaLocalDTO);
		estadisticaController.sumarLibreFallado(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarLibreAnotado(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(3, estadisticaActual.getTirosLibresAnotados());
		assertEquals(75, estadisticaActual.getTirosLibresPorcentaje(), DELTA);
		
		assertEquals(3, estadisticaActual.getPartido().getPuntosLocal());
	}
	
	@Test
	public void testSumarTiroLibres_Visitante_ComprobacionEnPartido() {
		estadisticaController.sumarLibreAnotado(estadisticaVisitanteDTO);
		estadisticaController.sumarLibreAnotado(estadisticaVisitanteDTO);
		estadisticaController.sumarLibreFallado(estadisticaVisitanteDTO);
		estadisticaVisitanteDTO = estadisticaController.sumarLibreAnotado(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(3, estadisticaActual.getTirosLibresAnotados());
		assertEquals(75, estadisticaActual.getTirosLibresPorcentaje(), DELTA);
		
		assertEquals(3, estadisticaActual.getPartido().getPuntosVisitante());
	}
	
	@Test
	public void testIntegracion_Marcador() {
		estadisticaController.sumarDosAnotado(estadisticaLocalDTO);
		estadisticaController.sumarLibreAnotado(estadisticaLocalDTO);
		estadisticaController.sumarDosFallado(estadisticaLocalDTO);
		estadisticaController.sumarLibreFallado(estadisticaLocalDTO);
		estadisticaController.sumarLibreFallado(estadisticaLocalDTO);
		estadisticaController.sumarTripleFallado(estadisticaLocalDTO);
		estadisticaController.sumarTripleAnotado(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarLibreAnotado(estadisticaLocalDTO);
		estadisticaController.sumarLibreAnotado(estadisticaVisitanteDTO);
		estadisticaController.sumarLibreAnotado(estadisticaVisitanteDTO);
		estadisticaController.sumarLibreFallado(estadisticaVisitanteDTO);
		estadisticaVisitanteDTO = estadisticaController.sumarLibreAnotado(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(2, estadisticaActual.getTirosLibresAnotados());
		assertEquals(2, estadisticaActual.getTirosTriples());
		assertEquals(1, estadisticaActual.getTirosTriplesAnotados());
		assertEquals(2, estadisticaActual.getTirosDos());
		assertEquals(1, estadisticaActual.getTirosDosAnotados());
		assertEquals(7, estadisticaActual.getPartido().getPuntosLocal());
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(4, estadisticaActual.getTirosLibres());
		assertEquals(3, estadisticaActual.getTirosLibresAnotados());
		assertEquals(3, estadisticaActual.getPartido().getPuntosVisitante());
	}
	
	@Test
	public void testSumarFaltaCometida_Local() {
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(1, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante() {
		estadisticaVisitanteDTO = estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(1, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Local_ComprobacionConPartido() {
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(3, estadisticaActual.getPartido().getFcLocal());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante_ComprobacionConPartido() {
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaVisitanteDTO = estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(2, estadisticaActual.getPartido().getFcVisitante());
	}
	
	@Test
	public void testSumarFaltaCometida_Local_ComprobacionConPartido_BONUS_JugadorExpulsado() {
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(4, estadisticaActual.getPartido().getFcLocal());
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante_ComprobacionConPartido_BONUS_JugadorExpulsado() {
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaVisitanteDTO = estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(4, estadisticaActual.getPartido().getFcVisitante());
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Local_JugadorCincoFaltas(){
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaCometida_Visitante_JugadorCincoFaltas(){
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(5, estadisticaActual.getFaltasCometidas());
	}
	
	@Test
	public void testSumarFaltaRecibida_Local() {
		estadisticaLocalDTO = estadisticaController.sumarFaltaRecibida(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(3, estadisticaActual.getFaltasRecibidas());
	}
	
	@Test
	public void testSumarFaltaRecibida_Visitante() {
		estadisticaVisitanteDTO = estadisticaController.sumarFaltaRecibida(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(3, estadisticaActual.getFaltasRecibidas());
	}
	
	@Test
	public void testObtenerValoracion_Local() {
		int resultado = estadisticaController.obtenerValoracion(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
	}
	
	@Test
	public void testObtenerValoracion_Visitante() {
		int resultado = estadisticaController.obtenerValoracion(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
	}
	
	@Test
	public void testObtenerValoracion_Local_Integracion() {
		int resultado  = estadisticaController.obtenerValoracion(estadisticaLocalDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaLocalDTO = estadisticaController.sumarFaltaRecibida(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarFaltaRecibida(estadisticaLocalDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaLocalDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(4, estadisticaActual.getValoracion());
		assertEquals(4, resultado);
		
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarFaltaCometida(estadisticaLocalDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaLocalDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(1, estadisticaActual.getValoracion());
		assertEquals(1, resultado);
		
		estadisticaLocalDTO = estadisticaController.sumarTripleAnotado(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarTripleFallado(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarTripleFallado(estadisticaLocalDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaLocalDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaLocalDTO = estadisticaController.sumarLibreAnotado(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarLibreFallado(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarLibreFallado(estadisticaLocalDTO);
		estadisticaLocalDTO = estadisticaController.sumarLibreFallado(estadisticaLocalDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaLocalDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaLocalDTO.getId());
		
		assertEquals(0, estadisticaActual.getValoracion());
		assertEquals(0, resultado);
	}
	
	@Test
	public void testObtenerValoracion_Visitante_Integracion() {
		int resultado  = estadisticaController.obtenerValoracion(estadisticaVisitanteDTO);
		
		Estadistica estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaVisitanteDTO = estadisticaController.sumarFaltaRecibida(estadisticaVisitanteDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaVisitanteDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(3, estadisticaActual.getValoracion());
		assertEquals(3, resultado);
		
		estadisticaVisitanteDTO = estadisticaController.sumarFaltaCometida(estadisticaVisitanteDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaVisitanteDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(2, estadisticaActual.getValoracion());
		assertEquals(2, resultado);
		
		estadisticaVisitanteDTO = estadisticaController.sumarTripleAnotado(estadisticaVisitanteDTO);
		estadisticaVisitanteDTO = estadisticaController.sumarTripleAnotado(estadisticaVisitanteDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaVisitanteDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(8, estadisticaActual.getValoracion());
		assertEquals(8, resultado);
		
		estadisticaVisitanteDTO = estadisticaController.sumarLibreFallado(estadisticaVisitanteDTO);
		resultado = estadisticaController.obtenerValoracion(estadisticaVisitanteDTO);
		
		estadisticaActual = estadisticaRepository.read(estadisticaVisitanteDTO.getId());
		
		assertEquals(7, estadisticaActual.getValoracion());
		assertEquals(7, resultado);
	}
	
	@Test
	public void testListarEstadisticas() {
		Collection<EstadisticaDTO> listaEstadisticas = estadisticaController.listarEstadisticas();
		
		assertEquals(2, listaEstadisticas.size());
		
		claveEstadisticaVisitante = tHelper.generaEstadisticaVisitante(equipoLocal, equipoVisitante);
		estadisticaVisitante = estadisticaRepository.read(claveEstadisticaVisitante);
		estadisticaVisitanteDTO = converterEstadistica.converToDTO(estadisticaVisitante);
		
		claveEstadisticaVisitante = tHelper.generaEstadisticaVisitante(equipoLocal, equipoVisitante);
		estadisticaVisitante = estadisticaRepository.read(claveEstadisticaVisitante);
		estadisticaVisitanteDTO = converterEstadistica.converToDTO(estadisticaVisitante);
		
		listaEstadisticas = estadisticaController.listarEstadisticas();
		
		assertEquals(4, listaEstadisticas.size());
	}
	
	@Test
	public void testNuevaEstadistica_Local() {
		EstadisticaDTO estadisticaNuevaDTO = estadisticaController.nuevaEstadisticaEnBlanco(partido.getId(), jugadorLocal.getId());
		
		Estadistica estadisticaAniadida = estadisticaRepository.read(estadisticaNuevaDTO.getId());
		
		assertNotNull(estadisticaAniadida.getJugador().getEquipo());
	}
	
	@Test
	public void testNuevaEstadistica_Visitante() {
		EstadisticaDTO estadisticaNuevaDTO = estadisticaController.nuevaEstadisticaEnBlanco(partido.getId(), jugadorVisitante.getId());
		
		Estadistica estadisticaAniadida = estadisticaRepository.read(estadisticaNuevaDTO.getId());
		
		assertNotNull(estadisticaAniadida.getJugador().getEquipo());
	}
}