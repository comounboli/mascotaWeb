package mascota.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.controller.PartidoController;
import mascota.dominio.ConverterEquipoImpl;
import mascota.dominio.ConverterPartidoImpl;
import mascota.dominio.Equipo;
import mascota.dominio.EquipoDTO;
import mascota.dominio.Partido;
import mascota.dominio.PartidoDTO;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;
import mascota.repository.PartidoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class PartidoControllerTest {

	@Autowired
	private PartidoController partidoController;
	@Autowired
	private PartidoRepository partidoRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private ConverterPartidoImpl converterPartido;
	@Autowired
	private ConverterEquipoImpl converterEquipo;

	private Long clavePartido;
	private PartidoDTO partidoDTO;
	
	private Long claveEquipoLocal;
	private Equipo equipoLocal;
	private EquipoDTO equipoLocalDTO;
	private Long claveEquipoVisitante;
	private Equipo equipoVisitante;
	private EquipoDTO equipoVisitanteDTO;

	@Before
	public void setUp() throws Exception {
		claveEquipoLocal = tHelper.generaEquipo();
		equipoLocal = equipoRepository.read(claveEquipoLocal);
		equipoLocalDTO = converterEquipo.converToDTO(equipoLocal);
		claveEquipoVisitante = tHelper.generaEquipo();
		equipoVisitante = equipoRepository.read(claveEquipoVisitante);
		equipoVisitanteDTO = converterEquipo.converToDTO(equipoVisitante);
		
		clavePartido = tHelper.generaPartido(equipoLocal, equipoVisitante);
		partidoDTO = converterPartido.converToDTO(partidoRepository.read(clavePartido));
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Local() {
		boolean resultado = partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoLocalDTO.getId());
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(true, resultado);
		assertEquals(1, partidoModificado.getFtLocal());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Visitante() {
		boolean resultado = partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoVisitanteDTO.getId());
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(true, resultado);
		assertEquals(1, partidoModificado.getFtVisitante());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Local_ACero() {
		partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoLocalDTO.getId());
		boolean resultado = partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoLocalDTO.getId());
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getFtLocal());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Visitante_ACero() {
		partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoVisitanteDTO.getId());
		boolean resultado = partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoVisitanteDTO.getId());
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getFtVisitante());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Local_YaExpulsado() {
		partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoLocalDTO.getId());
		partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoLocalDTO.getId());
		boolean resultado = partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoLocalDTO.getId());
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(false, resultado);
		assertEquals(2, partidoModificado.getFtLocal());
	}
	
	@Test
	public void testSumarFaltaTecnicaEntrenador_Visitante_YaExpulsado() {
		partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoVisitanteDTO.getId());
		partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoVisitanteDTO.getId());
		boolean resultado = partidoController.sumarFaltaTecnicaEntrenador(partidoDTO, equipoVisitanteDTO.getId());
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(false, resultado);
		assertEquals(2, partidoModificado.getFtVisitante());
	}
	
	@Test
	public void testPedirTM_Local() {
		boolean resultado = partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getTmLocal());
	}
	
	@Test
	public void testPedirTM_Visitante() {
		boolean resultado = partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(true, resultado);
		assertEquals(2, partidoModificado.getTmVisitante());
	}
	
	@Test
	public void testPedirTM_Local_ACero() {
		partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		boolean resultado = partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(0, partidoModificado.getTmLocal());
		assertEquals(true, resultado);
	} 
	
	@Test
	public void testPedirTM_Visitante_ACero() {
		partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		boolean resultado = partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(0, partidoModificado.getTmVisitante());
		assertEquals(true, resultado);
	} 
	
	@Test
	public void testPedirTM_YaConsumidos_Local() {
		partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		boolean resultado = partidoController.pedirTM(partidoDTO, claveEquipoLocal);
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(0, partidoModificado.getTmLocal());
		assertEquals(false, resultado);
	} 
	
	@Test
	public void testPedirTM_YaConsumidos_Visitante() {
		partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		boolean resultado = partidoController.pedirTM(partidoDTO, claveEquipoVisitante);
		
		Partido partidoModificado = partidoRepository.read(partidoDTO.getId());
		
		assertEquals(0, partidoModificado.getTmVisitante());
		assertEquals(false, resultado);
	} 

	@Test
	public void testNuevoPartido() {
		PartidoDTO partidoNuevo = partidoController.nuevoPartidoEnBlanco(claveEquipoLocal, claveEquipoVisitante);
		
		Partido partidoAniadido = partidoRepository.read(partidoNuevo.getId());
		
		assertEquals(0, partidoAniadido.getPuntosLocal());
	}
	
	@Test
	public void testModificarPartido() {
		PartidoDTO partidoAniadido = partidoController.modificarPartido(partidoDTO, claveEquipoLocal, claveEquipoVisitante);

		assertNotNull(partidoAniadido.getEquipoVisitante());
	}

	@Test
	public void testBorrarPartido_Integracion() {
		Collection<PartidoDTO> listaPartidos = partidoController.listarPartidos();
		assertEquals(1, listaPartidos.size());
		
		boolean resultado = partidoController.borrarPartido(partidoDTO);
		
		listaPartidos = partidoController.listarPartidos();
		
		assertEquals(true, resultado);
		assertEquals(0, listaPartidos.size());
	}
	
	@Test
	public void testCambiarPosesion() {
		assertEquals(true, partidoDTO.isPosesionLocal());
		
		PartidoDTO partidoAniadido = partidoController.cambiarPosesion(partidoDTO);
		
		assertEquals(false, partidoAniadido.isPosesionLocal());
	}
}