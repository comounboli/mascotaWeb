package mascota.controller;

import static org.junit.Assert.assertEquals;

import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import mascota.controller.EquipoController;
import mascota.dominio.ConverterEquipoImpl;
import mascota.dominio.Equipo;
import mascota.dominio.EquipoDTO;
import mascota.helper.TestHelper;
import mascota.repository.EquipoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
		locations={
				"classpath:mascota/applicationContext.xml"
		})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
	TransactionalTestExecutionListener.class})
@Transactional
public class EquipoControllerTest {
	
	@Autowired
	private EquipoController equipoController;
	@Autowired
	private EquipoRepository equipoRepository;
	@Autowired
	private TestHelper tHelper;
	@Autowired
	private ConverterEquipoImpl converterEquipo;
	
	private Long claveEquipo;
	private EquipoDTO equipoDTO;

	@Before
	public void setUp() throws Exception {
		claveEquipo = tHelper.generaEquipo();
		equipoDTO = converterEquipo.converToDTO(equipoRepository.read(claveEquipo));
	}

	@Test
	public void testNuevoEquipo() {
		EquipoDTO equipoNuevo = equipoController.nuevoEquipo(equipoDTO);
		
		Equipo equipoAniadido = equipoRepository.read(equipoNuevo.getId());
		
		assertEquals("Daygon", equipoAniadido.getNombreEquipo());
	}

	@Test
	public void testModificarEquipo() {
		EquipoDTO equipoNuevo = equipoController.modificarEquipo(equipoDTO, equipoDTO.getEntrenador(), "CBT");
				
		Equipo equipoAniadido = equipoRepository.read(equipoNuevo.getId());
		
		assertEquals("CBT", equipoAniadido.getNombreEquipo());;
	}
	
	@Test
	public void testBorrarEquipo_Integracion() {
		Collection<EquipoDTO> listaEquipos = equipoController.listarEquipos();
		
		assertEquals(1, listaEquipos.size());
		
		equipoController.borrarEquipo(equipoDTO);
		
		listaEquipos = equipoController.listarEquipos();

		assertEquals(0, listaEquipos.size());
	}
}