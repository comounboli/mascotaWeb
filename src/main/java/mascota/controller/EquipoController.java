package mascota.controller;

import java.util.Collection;

import mascota.dominio.EquipoDTO;

public interface EquipoController {

	EquipoDTO nuevoEquipo(EquipoDTO equipoDTO);

	void borrarEquipo(EquipoDTO equipoDTO);

	Collection<EquipoDTO> listarEquipos();

	EquipoDTO modificarEquipo(EquipoDTO equipoDTO, String nombreEntrenador, String nombreEquipo);
}