package mascota.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import mascota.dominio.ConverterPartidoImpl;
import mascota.dominio.Partido;
import mascota.dominio.PartidoDTO;
import mascota.repository.PartidoRepository;
import mascota.service.PartidoService;

@Controller
public class PartidoControllerImpl implements PartidoController, Serializable{

	private static final long serialVersionUID = 670184748597553142L;
	
	@Autowired
	private ConverterPartidoImpl converterPartido;
	@Autowired 
	private PartidoService partidoService;
	@Autowired
	private PartidoRepository partidoRepository;
	
	@Override
	public boolean pedirTM(PartidoDTO partidoDTO, long equipoId){
		return partidoService.pedirTM(partidoRepository.read(partidoDTO.getId()), equipoId);
	}

	@Override
	public boolean borrarPartido(PartidoDTO partidoDTO){
		Partido partido = converterPartido.converToENTITY(partidoDTO);
		
		return partidoService.borrarPartido(partido);
	}

	@Override
	public Collection<PartidoDTO> listarPartidos(){
		Collection<Partido> partido = partidoService.listarPartidos();
		Collection<PartidoDTO> resultado = new ArrayList<>();
		
		for (Partido p: partido) {
			resultado.add(converterPartido.converToDTO(p));
		}
		return resultado;
	}

	@Override
	public boolean sumarFaltaTecnicaEntrenador(PartidoDTO partidoDTO, long equipoId){
		return partidoService.sumarFaltaTecnicaEntrenador(partidoRepository.read(partidoDTO.getId()), equipoId);
	}

	@Override
	public PartidoDTO nuevoPartidoEnBlanco(long equipoLocalId, long equipoVisitanteId){		
		Partido partido = partidoService.nuevoPartidoEnBlanco(equipoLocalId, equipoVisitanteId);
		
		return converterPartido.converToDTO(partido);
	}

	@Override
	public PartidoDTO modificarPartido(PartidoDTO partidoDTO, long equipoLocalId, long equipoVisitanteId){
		Partido partido = partidoService.modificarPartido(partidoDTO.getId(), equipoLocalId, equipoVisitanteId);
		
		return converterPartido.converToDTO(partido);
	}
	
	@Override
	public PartidoDTO cambiarPosesion(PartidoDTO partidoDTO){
		Partido partido = partidoService.cambiarPosesion(partidoRepository.read(partidoDTO.getId()));
		
		return converterPartido.converToDTO(partido);
	}
}