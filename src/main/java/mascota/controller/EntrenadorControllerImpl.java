package mascota.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import mascota.dominio.ConverterEntrenadorImpl;
import mascota.dominio.Entrenador;
import mascota.dominio.EntrenadorDTO;
import mascota.service.EntrenadorService;

@Controller
public class EntrenadorControllerImpl implements EntrenadorController, Serializable{
	
	private static final long serialVersionUID = 2L;
	
	@Autowired
	private EntrenadorService entrenadorService;
	@Autowired
	private ConverterEntrenadorImpl converterEntrenador;
	
	@Override
	public EntrenadorDTO nuevoEntrenador(EntrenadorDTO entrenadorDTO){
		Entrenador entrenador = entrenadorService.nuevoEntrenador(entrenadorDTO.getNombreEntrenador());
		
		return converterEntrenador.converToDTO(entrenador);
	}

	@Override
	public EntrenadorDTO modificarEntrenador(EntrenadorDTO entrenadorDTO, String nombre){	
		Entrenador entrenador = entrenadorService.modificarEntrenador(entrenadorDTO.getId(), nombre);
		
		return converterEntrenador.converToDTO(entrenador);
		
	}
	
	@Override
	public void borrarEntrenador(EntrenadorDTO entrenadorDTO){
		Entrenador entrenador = converterEntrenador.converToENTITY(entrenadorDTO);
		
		entrenadorService.borrarEntrenador(entrenador.getId());
	}

	@Override
	public Collection<EntrenadorDTO> listarEntrenadores(){
		Collection<Entrenador> entrenador = entrenadorService.listarEntrenadores();
		Collection<EntrenadorDTO> resultado = new ArrayList<>();
		
		for (Entrenador e: entrenador) {
			resultado.add(converterEntrenador.converToDTO(e));
		}
		return resultado;
	}
}