package mascota.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import mascota.dominio.ConverterJugadorImpl;
import mascota.dominio.Jugador;
import mascota.dominio.JugadorDTO;
import mascota.repository.EquipoRepository;
import mascota.service.JugadorService;

@Controller
public class JugadorControllerImpl implements JugadorController, Serializable{

	private static final long serialVersionUID = -3112077466379384049L;
	
	@Autowired
	private ConverterJugadorImpl converterJugador;
	@Autowired 
	private JugadorService jugadorService;
	@Autowired
	private EquipoRepository equipoRepository;
	
	@Override
	public JugadorDTO nuevoJugador (JugadorDTO jugadorDTO){
		Jugador jugador = converterJugador.converToENTITY(jugadorDTO);
				
		Jugador nuevoJugador = jugadorService.nuevoJugador(jugador.getEquipo(), jugadorDTO.getNombreJugador(), jugadorDTO.getDorsal(), jugadorDTO.isCapitan());
		
		return converterJugador.converToDTO(nuevoJugador);
	}

	@Override
	public JugadorDTO modificarJugador(JugadorDTO jugadorDTO, String nombreEquipo, String nombreJugador, int dorsal, boolean capitan){
		jugadorDTO.setNombreEquipo(nombreEquipo);
		Jugador jugador = converterJugador.converToENTITY(jugadorDTO);
		
		Jugador nuevoJugador = jugadorService.modificarJugador(jugadorDTO.getId(), jugador.getEquipo(), nombreJugador, dorsal, capitan);
		
		return converterJugador.converToDTO(nuevoJugador);	
	}
	
	@Override
	public void borrarJugador(JugadorDTO jugadorDTO){
		Jugador jugador = converterJugador.converToENTITY(jugadorDTO);
		
		jugadorService.borrarJugador(jugador.getId());
	}

	@Override
	public Collection<JugadorDTO> listarJugadores(){
		Collection<Jugador> jugador = jugadorService.listarJugadores();
		Collection<JugadorDTO> resultado = new ArrayList<>();
		
		for (Jugador j: jugador) {
			resultado.add(converterJugador.converToDTO(j));
		}
		return resultado;
	}
}