package mascota.controller;

import java.util.Collection;

import mascota.dominio.JugadorDTO;

public interface JugadorController {

	JugadorDTO nuevoJugador(JugadorDTO jugadorDTO);

	void borrarJugador(JugadorDTO jugadorDTO);

	Collection<JugadorDTO> listarJugadores();

	JugadorDTO modificarJugador(JugadorDTO jugadorDTO, String nombreEquipo, String nombreJugador, int dorsal,
			boolean capitan);
}