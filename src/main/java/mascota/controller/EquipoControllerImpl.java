package mascota.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import mascota.dominio.ConverterEquipoImpl;
import mascota.dominio.Equipo;
import mascota.dominio.EquipoDTO;
import mascota.service.EquipoService;

@Controller
public class EquipoControllerImpl implements EquipoController, Serializable{

	private static final long serialVersionUID = -5892361626597151010L;
	
	@Autowired
	private ConverterEquipoImpl converterEquipo;
	@Autowired 
	private EquipoService equipoService;
	
	@Override
	public EquipoDTO nuevoEquipo(EquipoDTO equipoDTO){
		Equipo equipo = converterEquipo.converToENTITY(equipoDTO);
		Equipo nuevoEquipo = equipoService.nuevoEquipo(equipo.getEntrenador(),equipoDTO.getNombreEquipo());
		
		return converterEquipo.converToDTO(nuevoEquipo);
	}

	@Override
	public EquipoDTO modificarEquipo(EquipoDTO equipoDTO, String nombreEntrenador, String nombreEquipo){
		Equipo equipo = converterEquipo.converToENTITY(equipoDTO);
		Equipo modificadoEquipo = equipoService.modificarEquipo(equipoDTO.getId(), equipo.getEntrenador(), nombreEquipo);
		
		return converterEquipo.converToDTO(modificadoEquipo);	
	}
	
	@Override
	public void borrarEquipo(EquipoDTO equipoDTO){
		Equipo equipo = converterEquipo.converToENTITY(equipoDTO);
		
		equipoService.borrarEquipo(equipo.getId());
	}

	@Override
	public Collection<EquipoDTO> listarEquipos(){
		Collection<Equipo> equipo = equipoService.listarEquipos();
		Collection<EquipoDTO> resultado = new ArrayList<>();
		
		for (Equipo e: equipo) {
			resultado.add(converterEquipo.converToDTO(e));
		}
		return resultado;
	}
}