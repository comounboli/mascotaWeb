package mascota.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import mascota.dominio.ConverterEstadisticaImpl;
import mascota.dominio.Estadistica;
import mascota.dominio.EstadisticaDTO;
import mascota.repository.EstadisticaRepository;
import mascota.service.EstadisticaService;

@Controller
public class EstadisticaControllerImpl implements EstadisticaController {
	
	@Autowired
	private ConverterEstadisticaImpl converterEstadistica;
	@Autowired 
	private EstadisticaService estadisticaService;
	@Autowired
	private EstadisticaRepository estadisticaRepository;
	
	@Override
	public EstadisticaDTO sumarDosAnotado(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarDosAnotado(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}

	@Override
	public EstadisticaDTO sumarDosFallado(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarDosFallado(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}
	
	@Override
	public EstadisticaDTO sumarTripleAnotado(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarTripleAnotado(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}

	@Override
	public EstadisticaDTO sumarTripleFallado(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarTripleFallado(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}
	
	@Override
	public EstadisticaDTO sumarLibreAnotado(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarLibreAnotado(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}

	@Override
	public EstadisticaDTO sumarLibreFallado(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarLibreFallado(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}

	@Override
	public EstadisticaDTO sumarFaltaCometida(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarFaltaCometida(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}
	
	@Override
	public EstadisticaDTO sumarFaltaRecibida(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = estadisticaService.sumarFaltaRecibida(estadisticaRepository.read(estadisticaDTO.getId()));
		
		return converterEstadistica.converToDTO(estadistica);
	}
	
	@Override
	public int obtenerValoracion(EstadisticaDTO estadisticaDTO){
		Estadistica estadistica = converterEstadistica.converToENTITY(estadisticaDTO);
		
		estadisticaDTO.setValoracion(estadisticaService.obtenerValoracion(estadistica));
		
		return estadisticaService.obtenerValoracion(estadistica);
	}

	@Override
	public Collection<EstadisticaDTO> listarEstadisticas(){
		Collection<Estadistica> estadistica = estadisticaService.listarEstadisticas();
		Collection<EstadisticaDTO> resultado = new ArrayList<>();
		
		for (Estadistica p: estadistica) {
			resultado.add(converterEstadistica.converToDTO(p));
		}
		return resultado;
	}

	@Override
	public EstadisticaDTO nuevaEstadisticaEnBlanco(long partidoId, long jugadorId){
		Estadistica estadistica = estadisticaService.nuevaEstadisticaEnBlanco(partidoId, jugadorId);
		
		return converterEstadistica.converToDTO(estadistica);
	}
}