package mascota.controller;

import java.util.Collection;

import mascota.dominio.EntrenadorDTO;

public interface EntrenadorController {

	EntrenadorDTO nuevoEntrenador(EntrenadorDTO entrenadorDTO);

	EntrenadorDTO modificarEntrenador(EntrenadorDTO entrenadorDTO, String nombre);

	void borrarEntrenador(EntrenadorDTO entrenadorDTO);

	Collection<EntrenadorDTO> listarEntrenadores();
}