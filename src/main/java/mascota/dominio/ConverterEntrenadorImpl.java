package mascota.dominio;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class ConverterEntrenadorImpl implements Converter<EntrenadorDTO, Entrenador>, Serializable{
	
	@Override
	public EntrenadorDTO converToDTO(Entrenador entity) {
		EntrenadorDTO entrenadorDTO = new EntrenadorDTO();
		
		entrenadorDTO.setId(entity.getId());
		entrenadorDTO.setNombreEntrenador(entity.getNombreEntrenador());

		return entrenadorDTO;
	}

	@Override
	public Entrenador converToENTITY(EntrenadorDTO dto) {
		Entrenador entrenador = new Entrenador();
		
		entrenador.setId(dto.getId());
		entrenador.setNombreEntrenador(dto.getNombreEntrenador());

		return entrenador;
	}
}