package mascota.dominio;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mascota.repository.EntrenadorRepository;

@Component
public class ConverterEquipoImpl implements Converter<EquipoDTO, Equipo>, Serializable{
	
	@Autowired
	private EntrenadorRepository entrenadorRepository;
	
	@Override
	public EquipoDTO converToDTO(Equipo entity) {
		EquipoDTO equipoDTO = new EquipoDTO();
		
		equipoDTO.setId(entity.getId());
		equipoDTO.setNombreEquipo(entity.getNombreEquipo());
		equipoDTO.setEntrenador(entity.getEntrenador().getNombreEntrenador());

		return equipoDTO;
	}

	@Override
	public Equipo converToENTITY(EquipoDTO dto) {
		Equipo equipo = new Equipo();
		
		equipo.setId(dto.getId());
		equipo.setNombreEquipo(dto.getNombreEquipo());
		
		String aux = dto.getEntrenador();
		for (Entrenador e : entrenadorRepository.list()){
			if (e.getNombreEntrenador() == aux){
				equipo.setEntrenador(e);
			}
		}
		return equipo;
	}
}