package mascota.dominio;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mascota.repository.JugadorRepository;
import mascota.repository.PartidoRepository;

@Component
public class ConverterEstadisticaImpl implements Converter<EstadisticaDTO, Estadistica>, Serializable{
	
	@Autowired
	private PartidoRepository partidoRepository;
	@Autowired
	private JugadorRepository jugadorRepository;
	
	@Override
	public EstadisticaDTO converToDTO(Estadistica entity) {
		EstadisticaDTO estadisticaDTO = new EstadisticaDTO();
		
		estadisticaDTO.setId(entity.getId());
		estadisticaDTO.setIdPartido(entity.getPartido().getId());
		estadisticaDTO.setNombreJugador(entity.getJugador().getNombreJugador());
		estadisticaDTO.setTirosDos(entity.getTirosDos());
		estadisticaDTO.setTirosDosAnotados(entity.getTirosDosAnotados());
		estadisticaDTO.setTirosDosPorcentaje(entity.getTirosDosPorcentaje());
		estadisticaDTO.setTirosTriples(entity.getTirosTriples());
		estadisticaDTO.setTirosTriplesAnotados(entity.getTirosTriplesAnotados());
		estadisticaDTO.setTirosTriplesPorcentaje(entity.getTirosTriplesPorcentaje());
		estadisticaDTO.setTirosLibres(entity.getTirosLibres());
		estadisticaDTO.setTirosLibresAnotados(entity.getTirosLibresAnotados());
		estadisticaDTO.setTirosLibresPorcentaje(entity.getTirosLibresPorcentaje());
		estadisticaDTO.setFaltasCometidas(entity.getFaltasCometidas());
		estadisticaDTO.setFaltasRecibidas(entity.getFaltasRecibidas());
		estadisticaDTO.setValoracion(entity.getValoracion());

		return estadisticaDTO;
	}

	@Override
	public Estadistica converToENTITY(EstadisticaDTO dto) {
		Estadistica estadistica = new Estadistica();
		
		estadistica.setId(dto.getId());
		
		long aux = dto.getIdPartido();
		for (Partido p : partidoRepository.list()){
			if (p.getId() == aux){
				estadistica.setPartido(p);
			}
		}
		
		String aux2 = dto.getNombreJugador();
		for (Jugador j : jugadorRepository.list()){
			if (j.getNombreJugador() == aux2){
				estadistica.setJugador(j);
			}
		}

		estadistica.setTirosDos(dto.getTirosDos());
		estadistica.setTirosDosAnotados(dto.getTirosDosAnotados());
		estadistica.setTirosDosPorcentaje(dto.getTirosDosPorcentaje());
		estadistica.setTirosTriples(dto.getTirosTriples());
		estadistica.setTirosTriplesAnotados(dto.getTirosTriplesAnotados());
		estadistica.setTirosTriplesPorcentaje(dto.getTirosTriplesPorcentaje());
		estadistica.setTirosLibres(dto.getTirosLibres());
		estadistica.setTirosLibresAnotados(dto.getTirosLibresAnotados());
		estadistica.setTirosLibresPorcentaje(dto.getTirosLibresPorcentaje());
		estadistica.setFaltasCometidas(dto.getFaltasCometidas());
		estadistica.setFaltasRecibidas(dto.getFaltasRecibidas());
		estadistica.setValoracion(dto.getValoracion());

		return estadistica;
	}
}