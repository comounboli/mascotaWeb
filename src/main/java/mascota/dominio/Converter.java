package mascota.dominio;

public interface Converter<DTO,ENTITY>{

	public DTO converToDTO(ENTITY entity);
	public ENTITY converToENTITY(DTO dto);
}