package mascota.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import mascota.repository.Identificable;

@Entity
@Table(name="PARTIDO")
public class Partido implements Identificable<Long> {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@JoinColumn(name="ID_EQUIPO_LOCAL")
	@ManyToOne(fetch=FetchType.LAZY)
	private Equipo equipoLocal;
	
	@JoinColumn(name="ID_EQUIPO_VISITANTE")
	@ManyToOne(fetch=FetchType.LAZY)
	private Equipo equipoVisitante;
	
	@Column(name="MARCADOR_LOCAL")
	private int puntosLocal;
	
	@Column(name="MARCADOR_VISITANTE")
	private int puntosVisitante;
	
	@Column(name="TM_LOCAL")
	private int tmLocal;
	
	@Column(name="TM_VISITANTE")
	private int tmVisitante;
	
	@Column(name="FC_LOCAL")
	private int fcLocal;
	
	@Column(name="FC_VISITANTE")
	private int fcVisitante;
	
	@Column(name="FT_LOCAL")
	private int ftLocal;
	
	@Column(name="FT_VISITANTE")
	private int ftVisitante;
	
	@Column(name="POSESION_LOCAL")
	private boolean posesionLocal;

	@OneToMany(mappedBy="partido") 
	private List<Estadistica> listaEstadisticas = new ArrayList<>();

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Equipo getEquipoLocal() {
		return equipoLocal;
	}

	public void setEquipoLocal(Equipo equipoLocal) {
		this.equipoLocal = equipoLocal;
	}

	public Equipo getEquipoVisitante() {
		return equipoVisitante;
	}

	public void setEquipoVisitante(Equipo equipoVisitante) {
		this.equipoVisitante = equipoVisitante;
	}

	public int getPuntosLocal() {
		return puntosLocal;
	}

	public void setPuntosLocal(int puntosLocal) {
		this.puntosLocal = puntosLocal;
	}

	public int getPuntosVisitante() {
		return puntosVisitante;
	}

	public void setPuntosVisitante(int puntosVisitante) {
		this.puntosVisitante = puntosVisitante;
	}

	public int getTmLocal() {
		return tmLocal;
	}

	public void setTmLocal(int tmLocal) {
		this.tmLocal = tmLocal;
	}

	public int getTmVisitante() {
		return tmVisitante;
	}

	public void setTmVisitante(int tmVisitante) {
		this.tmVisitante = tmVisitante;
	}

	public int getFcLocal() {
		return fcLocal;
	}

	public void setFcLocal(int fcLocal) {
		this.fcLocal = fcLocal;
	}

	public int getFcVisitante() {
		return fcVisitante;
	}

	public void setFcVisitante(int fcVisitante) {
		this.fcVisitante = fcVisitante;
	}

	public int getFtLocal() {
		return ftLocal;
	}

	public void setFtLocal(int ftLocal) {
		this.ftLocal = ftLocal;
	}

	public int getFtVisitante() {
		return ftVisitante;
	}

	public void setFtVisitante(int ftVisitante) {
		this.ftVisitante = ftVisitante;
	}
	
	
	public boolean isPosesionLocal() {
		return posesionLocal;
	}

	public void setPosesionLocal(boolean posesionLocal) {
		this.posesionLocal = posesionLocal;
	}
}