package mascota.dominio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import mascota.repository.Identificable;

@Entity
@Table(name="EQUIPO")
public class Equipo implements Identificable<Long> {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@JoinColumn(name="ID_ENTRENADOR")
	@OneToOne(fetch=FetchType.LAZY)
	private Entrenador entrenador;

	@Column(name="NOMBRE_EQUIPO")
	private String nombreEquipo;
	
	@OneToMany(mappedBy="equipoLocal") 
	private List<Partido> listaPartidosLocal = new ArrayList<>();
	
	@OneToMany(mappedBy="equipoVisitante") 
	private List<Partido> listaPartidosVisitante = new ArrayList<>();
	
	@OneToMany(mappedBy="equipo") 
	private List<Jugador> listaJugadores = new ArrayList<>();

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Entrenador getEntrenador() {
		return entrenador;
	}

	public void setEntrenador(Entrenador entrenador) {
		this.entrenador = entrenador;
	}

	public String getNombreEquipo() {
		return nombreEquipo;
	}

	public void setNombreEquipo(String nombreEquipo) {
		this.nombreEquipo = nombreEquipo;
	}
}