package mascota.dominio;

public class PartidoDTO {

	private Long id;

	private String equipoLocal;

	private String equipoVisitante;

	private int puntosLocal;

	private int puntosVisitante;

	private int tmLocal;

	private int tmVisitante;

	private int fcLocal;

	private int fcVisitante;

	private int ftLocal;

	private int ftVisitante;
	
	private boolean posesionLocal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEquipoLocal() {
		return equipoLocal;
	}

	public void setEquipoLocal(String equipoLocal) {
		this.equipoLocal = equipoLocal;
	}

	public String getEquipoVisitante() {
		return equipoVisitante;
	}

	public void setEquipoVisitante(String equipoVisitante) {
		this.equipoVisitante = equipoVisitante;
	}

	public int getPuntosLocal() {
		return puntosLocal;
	}

	public void setPuntosLocal(int puntosLocal) {
		this.puntosLocal = puntosLocal;
	}

	public int getPuntosVisitante() {
		return puntosVisitante;
	}

	public void setPuntosVisitante(int puntosVisitante) {
		this.puntosVisitante = puntosVisitante;
	}

	public int getTmLocal() {
		return tmLocal;
	}

	public void setTmLocal(int tmLocal) {
		this.tmLocal = tmLocal;
	}

	public int getTmVisitante() {
		return tmVisitante;
	}

	public void setTmVisitante(int tmVisitante) {
		this.tmVisitante = tmVisitante;
	}

	public int getFcLocal() {
		return fcLocal;
	}

	public void setFcLocal(int fcLocal) {
		this.fcLocal = fcLocal;
	}

	public int getFcVisitante() {
		return fcVisitante;
	}

	public void setFcVisitante(int fcVisitante) {
		this.fcVisitante = fcVisitante;
	}

	public int getFtLocal() {
		return ftLocal;
	}

	public void setFtLocal(int ftLocal) {
		this.ftLocal = ftLocal;
	}

	public int getFtVisitante() {
		return ftVisitante;
	}

	public void setFtVisitante(int ftVisitante) {
		this.ftVisitante = ftVisitante;
	}

	public boolean isPosesionLocal() {
		return posesionLocal;
	}

	public void setPosesionLocal(boolean posesionLocal) {
		this.posesionLocal = posesionLocal;
	}
}