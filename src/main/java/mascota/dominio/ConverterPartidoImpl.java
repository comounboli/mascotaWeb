package mascota.dominio;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mascota.repository.EquipoRepository;

@Component
public class ConverterPartidoImpl implements Converter<PartidoDTO, Partido>, Serializable{

	private static final long serialVersionUID = 8211442876954196106L;
	
	@Autowired
	private EquipoRepository equipoRepository;
	
	@Override
	public PartidoDTO converToDTO(Partido entity) {
		PartidoDTO partidoDTO = new PartidoDTO();
		
		partidoDTO.setId(entity.getId());
		partidoDTO.setEquipoLocal(entity.getEquipoLocal().getNombreEquipo());
		partidoDTO.setEquipoVisitante(entity.getEquipoVisitante().getNombreEquipo());
		partidoDTO.setPuntosLocal(entity.getPuntosLocal());
		partidoDTO.setPuntosVisitante(entity.getPuntosVisitante());
		partidoDTO.setTmLocal(entity.getTmLocal());
		partidoDTO.setTmVisitante(entity.getTmVisitante());
		partidoDTO.setFcLocal(entity.getFcLocal());
		partidoDTO.setFcVisitante(entity.getFcVisitante());
		partidoDTO.setFtLocal(entity.getFtLocal());
		partidoDTO.setFtVisitante(entity.getFtVisitante());
		partidoDTO.setPosesionLocal(entity.isPosesionLocal());

		return partidoDTO;
	}

	@Override
	public Partido converToENTITY(PartidoDTO dto) {
		Partido partido = new Partido();
		
		partido.setId(dto.getId());
		
		String aux = dto.getEquipoLocal();
		for (Equipo e : equipoRepository.list()){
			if (e.getNombreEquipo() == aux){
				partido.setEquipoLocal(e);
			}
		}
		
		String aux2 = dto.getEquipoVisitante();
		for (Equipo e : equipoRepository.list()){
			if (e.getNombreEquipo() == aux2){
				partido.setEquipoVisitante(e);
			}
		}

		partido.setPuntosLocal(dto.getPuntosLocal());
		partido.setPuntosVisitante(dto.getPuntosVisitante());
		partido.setTmLocal(dto.getTmLocal());
		partido.setTmVisitante(dto.getTmVisitante());
		partido.setFcLocal(dto.getFcLocal());
		partido.setFcVisitante(dto.getFcVisitante());
		partido.setFtLocal(dto.getFtLocal());
		partido.setFtVisitante(dto.getFtVisitante());
		partido.setPosesionLocal(dto.isPosesionLocal());

		return partido;
	}
}