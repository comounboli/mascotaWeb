package mascota.dominio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mascota.repository.Identificable;

@Entity
@Table(name="ESTADISTICA")
public class Estadistica implements Identificable<Long> {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@JoinColumn(name="ID_PARTIDO")
	@ManyToOne(fetch=FetchType.LAZY)
	private Partido partido;
	
	@JoinColumn(name="ID_JUGADOR")
	@ManyToOne(fetch=FetchType.LAZY)
	private Jugador jugador;

	@Column(name="T2")
	private int tirosDos;
	
	@Column(name="T2_ANOTADOS")
	private int tirosDosAnotados;
	
	@Column(name="T2_PORCENTAJE")
	private double tirosDosPorcentaje;
	
	@Column(name="T3")
	private int tirosTriples;
	
	@Column(name="T3_ANOTADOS")
	private int tirosTriplesAnotados;
	
	@Column(name="T3_PORCENTAJE")
	private double tirosTriplesPorcentaje;
	
	@Column(name="TL")
	private int tirosLibres;
	
	@Column(name="TL_ANOTADOS")
	private int tirosLibresAnotados;
	
	@Column(name="TL_PORCENTAJE")
	private double tirosLibresPorcentaje;
	
	@Column(name="FC")
	private int faltasCometidas;
	
	@Column(name="FR")
	private int faltasRecibidas;
	
	@Column(name="VALORACION")
	private int valoracion;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public int getTirosDos() {
		return tirosDos;
	}

	public void setTirosDos(int tirosDos) {
		this.tirosDos = tirosDos;
	}

	public int getTirosDosAnotados() {
		return tirosDosAnotados;
	}

	public void setTirosDosAnotados(int tirosDosAnotados) {
		this.tirosDosAnotados = tirosDosAnotados;
	}

	public double getTirosDosPorcentaje() {
		return tirosDosPorcentaje;
	}

	public void setTirosDosPorcentaje(double tirosDosPorcentaje) {
		this.tirosDosPorcentaje = tirosDosPorcentaje;
	}

	public int getTirosTriples() {
		return tirosTriples;
	}

	public void setTirosTriples(int tirosTriples) {
		this.tirosTriples = tirosTriples;
	}

	public int getTirosTriplesAnotados() {
		return tirosTriplesAnotados;
	}

	public void setTirosTriplesAnotados(int tirosTriplesAnotados) {
		this.tirosTriplesAnotados = tirosTriplesAnotados;
	}

	public double getTirosTriplesPorcentaje() {
		return tirosTriplesPorcentaje;
	}

	public void setTirosTriplesPorcentaje(double tirosTriplesPorcentaje) {
		this.tirosTriplesPorcentaje = tirosTriplesPorcentaje;
	}

	public int getTirosLibres() {
		return tirosLibres;
	}

	public void setTirosLibres(int tirosLibres) {
		this.tirosLibres = tirosLibres;
	}

	public int getTirosLibresAnotados() {
		return tirosLibresAnotados;
	}

	public void setTirosLibresAnotados(int tirosLibresAnotados) {
		this.tirosLibresAnotados = tirosLibresAnotados;
	}

	public double getTirosLibresPorcentaje() {
		return tirosLibresPorcentaje;
	}

	public void setTirosLibresPorcentaje(double tirosLibresPorcentaje) {
		this.tirosLibresPorcentaje = tirosLibresPorcentaje;
	}

	public int getFaltasCometidas() {
		return faltasCometidas;
	}

	public void setFaltasCometidas(int faltasCometidas) {
		this.faltasCometidas = faltasCometidas;
	}

	public int getFaltasRecibidas() {
		return faltasRecibidas;
	}

	public void setFaltasRecibidas(int faltasRecibidas) {
		this.faltasRecibidas = faltasRecibidas;
	}

	public int getValoracion() {
		return valoracion;
	}

	public void setValoracion(int valoracion) {
		this.valoracion = valoracion;
	}
}