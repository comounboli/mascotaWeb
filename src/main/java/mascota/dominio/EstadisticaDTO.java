package mascota.dominio;

public class EstadisticaDTO {

	private Long id;

	private long idPartido;

	private String nombreJugador;

	private int tirosDos;

	private int tirosDosAnotados;

	private double tirosDosPorcentaje;

	private int tirosTriples;

	private int tirosTriplesAnotados;

	private double tirosTriplesPorcentaje;

	private int tirosLibres;

	private int tirosLibresAnotados;

	private double tirosLibresPorcentaje;

	private int faltasCometidas;

	private int faltasRecibidas;

	private int valoracion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public long getIdPartido() {
		return idPartido;
	}

	public void setIdPartido(long idPartido) {
		this.idPartido = idPartido;
	}

	public String getNombreJugador() {
		return nombreJugador;
	}

	public void setNombreJugador(String nombreJugador) {
		this.nombreJugador = nombreJugador;
	}

	public int getTirosDos() {
		return tirosDos;
	}

	public void setTirosDos(int tirosDos) {
		this.tirosDos = tirosDos;
	}

	public int getTirosDosAnotados() {
		return tirosDosAnotados;
	}

	public void setTirosDosAnotados(int tirosDosAnotados) {
		this.tirosDosAnotados = tirosDosAnotados;
	}

	public double getTirosDosPorcentaje() {
		return tirosDosPorcentaje;
	}

	public void setTirosDosPorcentaje(double tirosDosPorcentaje) {
		this.tirosDosPorcentaje = tirosDosPorcentaje;
	}

	public int getTirosTriples() {
		return tirosTriples;
	}

	public void setTirosTriples(int tirosTriples) {
		this.tirosTriples = tirosTriples;
	}

	public int getTirosTriplesAnotados() {
		return tirosTriplesAnotados;
	}

	public void setTirosTriplesAnotados(int tirosTriplesAnotados) {
		this.tirosTriplesAnotados = tirosTriplesAnotados;
	}

	public double getTirosTriplesPorcentaje() {
		return tirosTriplesPorcentaje;
	}

	public void setTirosTriplesPorcentaje(double tirosTriplesPorcentaje) {
		this.tirosTriplesPorcentaje = tirosTriplesPorcentaje;
	}

	public int getTirosLibres() {
		return tirosLibres;
	}

	public void setTirosLibres(int tirosLibres) {
		this.tirosLibres = tirosLibres;
	}

	public int getTirosLibresAnotados() {
		return tirosLibresAnotados;
	}

	public void setTirosLibresAnotados(int tirosLibresAnotados) {
		this.tirosLibresAnotados = tirosLibresAnotados;
	}

	public double getTirosLibresPorcentaje() {
		return tirosLibresPorcentaje;
	}

	public void setTirosLibresPorcentaje(double tirosLibresPorcentaje) {
		this.tirosLibresPorcentaje = tirosLibresPorcentaje;
	}

	public int getFaltasCometidas() {
		return faltasCometidas;
	}

	public void setFaltasCometidas(int faltasCometidas) {
		this.faltasCometidas = faltasCometidas;
	}

	public int getFaltasRecibidas() {
		return faltasRecibidas;
	}

	public void setFaltasRecibidas(int faltasRecibidas) {
		this.faltasRecibidas = faltasRecibidas;
	}

	public int getValoracion() {
		return valoracion;
	}

	public void setValoracion(int valoracion) {
		this.valoracion = valoracion;
	}
}