package mascota.dominio;

public class EquipoDTO {

	private Long id;

	private String nombreEntrenador;

	private String nombreEquipo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntrenador() {
		return nombreEntrenador;
	}

	public void setEntrenador(String entrenador) {
		this.nombreEntrenador = entrenador;
	}

	public String getNombreEquipo() {
		return nombreEquipo;
	}

	public void setNombreEquipo(String nombreEquipo) {
		this.nombreEquipo = nombreEquipo;
	}
}