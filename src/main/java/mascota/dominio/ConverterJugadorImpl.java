package mascota.dominio;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mascota.repository.EquipoRepository;

@Component
public class ConverterJugadorImpl implements Converter<JugadorDTO, Jugador>, Serializable{
	
	@Autowired
	private EquipoRepository equipoRepository;
	
	@Override
	public JugadorDTO converToDTO(Jugador entity) {
		JugadorDTO jugadorDTO = new JugadorDTO();
		
		jugadorDTO.setId(entity.getId());
		jugadorDTO.setNombreEquipo(entity.getEquipo().getNombreEquipo());
		jugadorDTO.setNombreJugador(entity.getNombreJugador());
		jugadorDTO.setCapitan(entity.isCapitan());
		jugadorDTO.setDorsal(entity.getDorsal());

		return jugadorDTO;
	}

	@Override
	public Jugador converToENTITY(JugadorDTO dto) {
		Jugador jugador = new Jugador();
		
		jugador.setId(dto.getId());
		
		String aux = dto.getNombreEquipo();
		for (Equipo e : equipoRepository.list()){
			if (e.getNombreEquipo() == aux){
				jugador.setEquipo(e);
			}
		}

		jugador.setNombreJugador(dto.getNombreJugador());
		jugador.setCapitan(dto.isCapitan());
		jugador.setDorsal(dto.getDorsal());

		return jugador;
	}
}