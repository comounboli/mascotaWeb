package mascota.service;

import java.io.Serializable;
import java.util.List;

import mascota.dominio.Equipo;
import mascota.dominio.Jugador;

public interface JugadorService extends Serializable{

	boolean borrarJugador(long jugadorId);

	List<Jugador> listarJugadores();

	Jugador nuevoJugador(Equipo equipoId, String nombre, int dorsal, boolean capitan);

	Jugador modificarJugador(long jugadorId, Equipo equipoId, String nombre, int dorsal, boolean capitan);
}