package mascota.service;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Estadistica;
import mascota.repository.EstadisticaRepository;
import mascota.repository.JugadorRepository;
import mascota.repository.PartidoRepository;

@Service
@Transactional
public class EstadisticaServiceImpl implements EstadisticaService {

	private static final long serialVersionUID = -3519562145782807193L;
	
	@Autowired 
	private EstadisticaRepository estadisticaRepository;
	@Autowired 
	private PartidoService partidoService;
	@Autowired 
	private PartidoRepository partidoRepository;
	@Autowired 
	private JugadorRepository jugadorRepository;
	
	@Override
	public Estadistica sumarDosAnotado(Estadistica estadistica){
		int tirosTotales = estadistica.getTirosDos() + 1;
		int tirosAnotadosTotales = estadistica.getTirosDosAnotados() + 1;
		double porcentaje = (double) tirosAnotadosTotales*100 / tirosTotales;

		estadistica.setTirosDos(tirosTotales);
		estadistica.setTirosDosAnotados(tirosAnotadosTotales);
		estadistica.setTirosDosPorcentaje(porcentaje);

		partidoService.sumarPuntos(estadistica.getId(), 2);
		estadisticaRepository.update(estadistica);
		return estadistica;
	}
	
	@Override
	public Estadistica sumarDosFallado(Estadistica estadistica){
		int tirosTotales = estadistica.getTirosDos() + 1;
		int tirosAnotadosTotales = estadistica.getTirosDosAnotados();
		double porcentaje = (double) tirosAnotadosTotales*100 / tirosTotales;

		estadistica.setTirosDos(tirosTotales);
		estadistica.setTirosDosAnotados(tirosAnotadosTotales);
		estadistica.setTirosDosPorcentaje(porcentaje);

		estadisticaRepository.update(estadistica);
		return estadistica;
	}

	@Override
	public Estadistica sumarTripleAnotado(Estadistica estadistica){
		int tirosTotales = estadistica.getTirosTriples() + 1;
		int tirosAnotadosTotales = estadistica.getTirosTriplesAnotados() + 1;
		double porcentaje = (double) tirosAnotadosTotales*100 / tirosTotales;

		estadistica.setTirosTriples(tirosTotales);
		estadistica.setTirosTriplesAnotados(tirosAnotadosTotales);
		estadistica.setTirosTriplesPorcentaje(porcentaje);

		partidoService.sumarPuntos(estadistica.getId(), 3);
		estadisticaRepository.update(estadistica);
		return estadistica;
	}
	
	@Override
	public Estadistica sumarTripleFallado(Estadistica estadistica){
		int tirosTotales = estadistica.getTirosTriples() + 1;
		int tirosAnotadosTotales = estadistica.getTirosTriplesAnotados();
		double porcentaje = (double) tirosAnotadosTotales*100 / tirosTotales;

		estadistica.setTirosTriples(tirosTotales);
		estadistica.setTirosTriplesAnotados(tirosAnotadosTotales);
		estadistica.setTirosTriplesPorcentaje(porcentaje);

		estadisticaRepository.update(estadistica);
		return estadistica;
	}

	@Override
	public Estadistica sumarLibreAnotado(Estadistica estadistica){
		int tirosTotales = estadistica.getTirosLibres() + 1;
		int tirosAnotadosTotales = estadistica.getTirosLibresAnotados() + 1;
		double porcentaje = (double) tirosAnotadosTotales*100 / tirosTotales;

		estadistica.setTirosLibres(tirosTotales);
		estadistica.setTirosLibresAnotados(tirosAnotadosTotales);
		estadistica.setTirosLibresPorcentaje(porcentaje);

		partidoService.sumarPuntos(estadistica.getId(), 1);
		estadisticaRepository.update(estadistica);
		return estadistica;
	}
	
	@Override
	public Estadistica sumarLibreFallado(Estadistica estadistica){
		int tirosTotales = estadistica.getTirosLibres() + 1;
		int tirosAnotadosTotales = estadistica.getTirosLibresAnotados();
		double porcentaje = (double) tirosAnotadosTotales*100 / tirosTotales;

		estadistica.setTirosLibres(tirosTotales);
		estadistica.setTirosLibresAnotados(tirosAnotadosTotales);
		estadistica.setTirosLibresPorcentaje(porcentaje);

		estadisticaRepository.update(estadistica);
		return estadistica;
	}

	@Override
	public Estadistica sumarFaltaCometida(Estadistica estadistica){
		Estadistica aux = estadistica;
		int cometidas = aux.getFaltasCometidas() + 1;

		aux = gestionarNumeroFaltas(aux, cometidas);

		partidoService.sumarFaltaEnPartido(aux.getId());
		estadisticaRepository.update(aux);
		return aux;
	}

	private Estadistica gestionarNumeroFaltas(Estadistica estadistica, int cometidas) {
		if (cometidas > 5){
			Logger.getLogger(getClass().getName()).log(
					Level.INFO, "Jugador ya expulsado");
		} else {
			estadistica.setFaltasCometidas(cometidas);
			if (cometidas == 5){
				Logger.getLogger(getClass().getName()).log(
						Level.INFO, "Quinta falta. Jugador expulsado.");
			}
		}
		return estadistica;
	}

	@Override
	public Estadistica sumarFaltaRecibida(Estadistica estadistica){
		estadistica.setFaltasRecibidas(estadistica.getFaltasRecibidas() + 1);

		estadisticaRepository.update(estadistica);
		return estadistica;
	}

	@Override
	public int obtenerValoracion(Estadistica estadistica){
		int resultado = 0;

		resultado -= (estadistica.getTirosDos() - estadistica.getTirosDosAnotados());
		resultado += estadistica.getTirosDosAnotados()*2;
		resultado -= (estadistica.getTirosTriples() - estadistica.getTirosTriplesAnotados());
		resultado += estadistica.getTirosTriplesAnotados()*3;
		resultado -= (estadistica.getTirosLibres() - estadistica.getTirosLibresAnotados());
		resultado += estadistica.getTirosLibresAnotados();
		resultado -= estadistica.getFaltasCometidas();
		resultado += estadistica.getFaltasRecibidas();

		estadistica.setValoracion(resultado);
		estadisticaRepository.update(estadistica);
		return resultado;
	}

	@Override
	public Collection<Estadistica> listarEstadisticas(){
		return estadisticaRepository.list();
	}
	
	@Override
	public Estadistica nuevaEstadisticaEnBlanco(long partidoId, long jugadorId){
		Estadistica nuevaEstadistica = new Estadistica();
		
		nuevaEstadistica.setPartido(partidoRepository.read(partidoId));
		nuevaEstadistica.setJugador(jugadorRepository.read(jugadorId));
		nuevaEstadistica.setTirosDos(0);
		nuevaEstadistica.setTirosDosAnotados(0);
		nuevaEstadistica.setTirosDosPorcentaje(0);
		nuevaEstadistica.setTirosTriples(0);
		nuevaEstadistica.setTirosTriplesAnotados(0);
		nuevaEstadistica.setTirosTriplesPorcentaje(0);
		nuevaEstadistica.setTirosLibres(0);
		nuevaEstadistica.setTirosLibresAnotados(0);
		nuevaEstadistica.setTirosLibresPorcentaje(0);
		nuevaEstadistica.setFaltasCometidas(0);
		nuevaEstadistica.setFaltasRecibidas(0);
		nuevaEstadistica.setValoracion(0);
		
		estadisticaRepository.add(nuevaEstadistica);
		return nuevaEstadistica;
	}
}