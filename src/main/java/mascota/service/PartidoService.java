package mascota.service;

import java.io.Serializable;
import java.util.Collection;

import mascota.dominio.Partido;

public interface PartidoService extends Serializable{

	boolean borrarPartido(Partido partido);

	Collection<Partido> listarPartidos();

	boolean sumarFaltaTecnicaEntrenador(Partido partido, long equipoId);

	boolean pedirTM(Partido partido, long equipoId);

	boolean sumarFaltaEnPartido(long estadisticaId);

	boolean sumarPuntos(long estadisticaId, int puntos);

	Partido nuevoPartidoEnBlanco(long equipoLocalId, long equipoVisitanteId);

	Partido modificarPartido(long partidoId, long equipoLocalId, long equipoVisitanteId);

	Partido cambiarPosesion(Partido partido);
}