package mascota.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Entrenador;
import mascota.dominio.Equipo;
import mascota.repository.EquipoRepository;

@Service
@Transactional
public class EquipoServiceImpl implements EquipoService {

	private static final long serialVersionUID = -4440340755313615587L;
	
	@Autowired 
	private EquipoRepository equipoRepository;

	@Override
	public Equipo nuevoEquipo(Entrenador entrenador, String nombre){
		Equipo nuevoEquipo = new Equipo();
		
		nuevoEquipo.setEntrenador(entrenador);
		nuevoEquipo.setNombreEquipo(nombre);
		
		equipoRepository.add(nuevoEquipo);
		
		return nuevoEquipo;
	}

	@Override
	public Equipo modificarEquipo(long equipoId, Entrenador entrenador, String nombre){
		Equipo modificadoEquipo = equipoRepository.read(equipoId);

		modificadoEquipo.setEntrenador(entrenador);
		modificadoEquipo.setNombreEquipo(nombre);

		equipoRepository.update(modificadoEquipo);
		return modificadoEquipo;
	}
	
	@Override
	public boolean borrarEquipo(long equipoId){
		boolean resultado = false;
		
		for (Equipo e : equipoRepository.list()){
			if(e.getId() == equipoId){
				equipoRepository.delete(e);
				resultado = true;
			}
		}
		return resultado;
	}
	
	@Override
	public List<Equipo> listarEquipos(){
		return equipoRepository.list();
	}
}