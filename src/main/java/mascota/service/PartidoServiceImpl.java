package mascota.service;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Estadistica;
import mascota.dominio.Jugador;
import mascota.dominio.Partido;
import mascota.repository.EquipoRepository;
import mascota.repository.EstadisticaRepository;
import mascota.repository.PartidoRepository;

@Service
@Transactional
public class PartidoServiceImpl implements PartidoService {

	private static final long serialVersionUID = -355898681938949211L;
	
	@Autowired 
	private PartidoRepository partidoRepository;
	@Autowired 
	private EstadisticaRepository estadisticaRepository;
	@Autowired 
	private EquipoRepository equipoRepository;
	
	@Override
	public boolean sumarFaltaTecnicaEntrenador(Partido partido, long equipoId){
		boolean resultado;
		
		if (partido.getEquipoLocal().getId() == equipoId){
			resultado = sumarLocal(partido);
		} else {
			resultado = sumarVisitante(partido);
		}
		return resultado;
	}
	
	private boolean sumarLocal(Partido partido){
		boolean resultado = false;
		int cometidas = partido.getFtLocal();
		
		if (!entrenadorExpulsado(cometidas)){
			partido.setFtLocal(cometidas + 1);
			resultado = true;
			if (cometidas == 1){
				Logger.getLogger(getClass().getName()).log(
						Level.INFO, "Entrenador expulsado por doble técnica");
			}	
		} else {
			Logger.getLogger(getClass().getName()).log(
					Level.INFO, "Entrenador ya expulsado");
		}
		partidoRepository.update(partido);
		return resultado;
	}
	
	private boolean sumarVisitante(Partido partido){
		boolean resultado = false;
		int cometidas = partido.getFtVisitante();
		
		if (!entrenadorExpulsado(cometidas)){
			partido.setFtVisitante(cometidas + 1);
			resultado = true;
			if (cometidas == 1){
				Logger.getLogger(getClass().getName()).log(
						Level.INFO, "Entrenador expulsado por doble técnica");
			}	
		} else {
			Logger.getLogger(getClass().getName()).log(
					Level.INFO, "Entrenador ya expulsado");
		}
		partidoRepository.update(partido);
		return resultado;
	}
	
	private boolean entrenadorExpulsado(int cometidas){
		return cometidas == 2;
	}
	
	@Override
	public boolean pedirTM(Partido partido, long equipoId){
		boolean resultado;
		
		if (partido.getEquipoLocal().getId() == equipoId){
			resultado = pedirLocal(partido);
		} else {
			resultado = pedirVisitante(partido);
		}
		return resultado;
	}
	
	private boolean pedirLocal(Partido partido){
		boolean resultado = false;
		int disponibles = partido.getTmLocal();

		if (hayTM(disponibles)){
			partido.setTmLocal(disponibles - 1);
			resultado = true;
			if (disponibles == 1){
				Logger.getLogger(getClass().getName()).log(
						Level.INFO, "Último Tiempo Muerto");
			}
		} else {
			Logger.getLogger(getClass().getName()).log(
					Level.INFO, "Tiempos muestros consumidos");
		}
		partidoRepository.update(partido);
		return resultado;
	}
	
	private boolean pedirVisitante(Partido partido){
		boolean resultado = false;
		int disponibles = partido.getTmVisitante();

		if (hayTM(disponibles)){
			partido.setTmVisitante(disponibles - 1);
			resultado = true;
			if (disponibles == 1){
				Logger.getLogger(getClass().getName()).log(
						Level.INFO, "Último Tiempo Muerto");	
			} 
		} else {
			Logger.getLogger(getClass().getName()).log(
					Level.INFO, "Tiempos muestros consumidos");
		}
		partidoRepository.update(partido);
		return resultado;
	}
	
	private boolean hayTM(int disponibles){
		return disponibles > 0;
	}
	
	@Override
	public boolean sumarFaltaEnPartido(long estadisticaId){
		Estadistica estadistica = estadisticaRepository.read(estadisticaId);
		boolean resultado;

		Partido partido = estadistica.getPartido();
		Jugador jugador = estadistica.getJugador();

		if (partido.getEquipoLocal() == jugador.getEquipo()){
			resultado = sumarFaltaLocal(partido);
		} else {
			resultado = sumarFaltaVisitante(partido);
		}
		return resultado;
	}
	
	private boolean sumarFaltaLocal (Partido partido){
		boolean resultado = false;
		int cometidas = partido.getFcLocal();

		if(!esBonus(cometidas)){
			partido.setFcLocal(cometidas + 1);
			resultado = true;
			if (cometidas + 1 == 4 ){
				Logger.getLogger(getClass().getName()).log(
						Level.INFO, "BONUS");
			}
		} else {
			Logger.getLogger(getClass().getName()).log(
					Level.INFO, "Dos tiros");
		}
		partidoRepository.update(partido);
		return resultado;
	}

	private boolean sumarFaltaVisitante (Partido partido){
		boolean resultado = false;
		int cometidas = partido.getFcVisitante();

		if(!esBonus(cometidas)){
			partido.setFcVisitante (cometidas + 1);
			resultado = true;
			if (cometidas + 1 == 4 ){
				Logger.getLogger(getClass().getName()).log(
						Level.INFO, "BONUS");
			}
		} else {
			Logger.getLogger(getClass().getName()).log(
					Level.INFO, "Dos tiros");
		}
		partidoRepository.update(partido);
		return resultado;
	}

	private boolean esBonus(int cometidas){
		return cometidas >= 4;
	}
	
	@Override
	public boolean sumarPuntos(long estadisticaId, int puntos){
		Estadistica estadistica = estadisticaRepository.read(estadisticaId);
		boolean resultado;
		
		Partido partido = estadistica.getPartido();
		Jugador jugador = estadistica.getJugador();
		
		if (partido.getEquipoLocal() == jugador.getEquipo()){
			sumaPuntosLocal(partido, puntos);
			resultado = true;
		} else {
			sumaPuntosVisitante(partido, puntos);
			resultado = true;
		}
		return resultado;
	}
	
	private void sumaPuntosLocal(Partido partido, int puntos){
		int aux = partido.getPuntosLocal() + puntos;
		
		partido.setPuntosLocal(aux);
		
		partidoRepository.update(partido);	
	}
	
	private void sumaPuntosVisitante(Partido partido, int puntos){
		int aux = partido.getPuntosVisitante() + puntos;
		
		partido.setPuntosVisitante(aux);
		
		partidoRepository.update(partido);	
	}
	
	@Override
	public Partido cambiarPosesion(Partido partido){
		if(partido.isPosesionLocal()){
			partido.setPosesionLocal(false);
		} else {
			partido.setPosesionLocal(true);
		}
		partidoRepository.update(partido);
		return partido;
	}

	@Override
	public Partido nuevoPartidoEnBlanco(long equipoLocalId, long equipoVisitanteId){
		Equipo local = equipoRepository.read(equipoLocalId);
		Equipo visitante = equipoRepository.read(equipoVisitanteId);
		Partido nuevoPartido = new Partido();
		
		nuevoPartido.setEquipoLocal(local);
		nuevoPartido.setEquipoVisitante(visitante);
		nuevoPartido.setTmLocal(5);
		nuevoPartido.setTmVisitante(5);
		nuevoPartido.setFcLocal(0);
		nuevoPartido.setFcVisitante(0);
		nuevoPartido.setFtLocal(0);
		nuevoPartido.setFtVisitante(0);
		nuevoPartido.setPosesionLocal(true);
		
		partidoRepository.add(nuevoPartido);
		return nuevoPartido;
	}
	
	@Override
	public Partido modificarPartido(long partidoId, long equipoLocalId, long equipoVisitanteId){
		Equipo local = equipoRepository.read(equipoLocalId);
		Equipo visitante = equipoRepository.read(equipoVisitanteId);
		Partido modificadoPartido = partidoRepository.read(partidoId);

		modificadoPartido.setEquipoLocal(local);
		modificadoPartido.setEquipoVisitante(visitante);

		partidoRepository.update(modificadoPartido);
		return modificadoPartido;
	}
	
	@Override
	public boolean borrarPartido(Partido partido){
		Partido partidoBorrar = partidoRepository.read(partido.getId());
		boolean resultado = false;
		
		if (partidoBorrar != null){
			partidoRepository.delete(partidoBorrar);
			resultado = true;
		}
		return resultado;
	}
	
	@Override
	public Collection<Partido> listarPartidos(){
		return partidoRepository.list();
	}
}