package mascota.service;

import java.io.Serializable;
import java.util.List;

import mascota.dominio.Entrenador;

public interface EntrenadorService extends Serializable{
	
	Entrenador nuevoEntrenador(String nombre);

	Entrenador modificarEntrenador(long entrenadorId, String nombre);
	
	boolean borrarEntrenador(long entrenadorId);

	List<Entrenador> listarEntrenadores();
}