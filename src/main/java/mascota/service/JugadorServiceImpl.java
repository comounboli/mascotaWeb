package mascota.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mascota.dominio.Equipo;
import mascota.dominio.Jugador;
import mascota.repository.JugadorRepository;

@Service
@Transactional
public class JugadorServiceImpl implements JugadorService {

	private static final long serialVersionUID = -362533192285040418L;
	
	@Autowired 
	private JugadorRepository jugadorRepository;

	@Override
	public Jugador nuevoJugador(Equipo equipoId, String nombre, int dorsal, boolean capitan){
		Jugador nuevoJugador = new Jugador();
		
		nuevoJugador.setEquipo(equipoId);
		nuevoJugador.setNombreJugador(nombre);
		nuevoJugador.setDorsal(dorsal);
		nuevoJugador.setCapitan(capitan);
		
		jugadorRepository.add(nuevoJugador);
		
		return nuevoJugador;
	}
	
	@Override
	public Jugador modificarJugador(long jugadorId, Equipo equipoId, String nombre, int dorsal, boolean capitan){
		Jugador modificadoJugador = jugadorRepository.read(jugadorId);

		modificadoJugador.setEquipo(equipoId);
		modificadoJugador.setNombreJugador(nombre);
		modificadoJugador.setDorsal(dorsal);
		modificadoJugador.setCapitan(capitan);

		jugadorRepository.update(modificadoJugador);
		return modificadoJugador;
	}
	
	@Override
	public boolean borrarJugador(long jugadorId){
		boolean resultado = false;
		
		for (Jugador j : jugadorRepository.list()){
			if(j.getId() == jugadorId){
				jugadorRepository.delete(j);
				resultado = true;
			}
		}
		return resultado;
	}
	
	@Override
	public List<Jugador> listarJugadores(){
		return jugadorRepository.list();
	}
}