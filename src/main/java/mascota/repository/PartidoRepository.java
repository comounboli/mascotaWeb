package mascota.repository;

import java.io.Serializable;

import mascota.dominio.Partido;

public interface PartidoRepository extends IRepository<Long, Partido>, Serializable{
}