package mascota.repository;

import org.springframework.stereotype.Repository;

import mascota.dominio.Jugador;

@Repository
public class JugadorRepositoryImpl extends AbstractRepositoryImpl<Long, Jugador> implements JugadorRepository {

	@Override
	public Class<Jugador> getClassDeT() {
		return Jugador.class;
	}

	@Override
	public String getNombreTabla() {
		return "JUGADOR";
	}
}