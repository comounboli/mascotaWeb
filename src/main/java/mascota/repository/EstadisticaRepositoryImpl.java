package mascota.repository;

import org.springframework.stereotype.Repository;

import mascota.dominio.Estadistica;

@Repository
public class EstadisticaRepositoryImpl extends AbstractRepositoryImpl<Long, Estadistica> implements EstadisticaRepository {

	@Override
	public Class<Estadistica> getClassDeT() {
		return Estadistica.class;
	}

	@Override
	public String getNombreTabla() {
		return "ESTADISTICA";
	}
}