package mascota.repository;

import org.springframework.stereotype.Repository;

import mascota.dominio.Entrenador;

@Repository
public class EntrenadorRepositoryImpl extends AbstractRepositoryImpl<Long, Entrenador> implements EntrenadorRepository {

	@Override
	public Class<Entrenador> getClassDeT() {
		return Entrenador.class;
	}

	@Override
	public String getNombreTabla() {
		return "ENTRENADOR";
	}
}