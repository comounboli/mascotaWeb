package mascota.repository;

import org.springframework.stereotype.Repository;

import mascota.dominio.Equipo;

@Repository
public class EquipoRepositoryImpl extends AbstractRepositoryImpl<Long, Equipo> implements EquipoRepository {

	@Override
	public Class<Equipo> getClassDeT() {
		return Equipo.class;
	}

	@Override
	public String getNombreTabla() {
		return "EQUIPO";
	}
}