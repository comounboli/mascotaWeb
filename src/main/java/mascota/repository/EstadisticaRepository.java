package mascota.repository;

import java.io.Serializable;

import mascota.dominio.Estadistica;

public interface EstadisticaRepository extends IRepository<Long, Estadistica>, Serializable{
}