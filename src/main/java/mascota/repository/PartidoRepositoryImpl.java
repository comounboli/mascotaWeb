package mascota.repository;

import org.springframework.stereotype.Repository;

import mascota.dominio.Partido;

@Repository
public class PartidoRepositoryImpl extends AbstractRepositoryImpl<Long, Partido> implements PartidoRepository {

	@Override
	public Class<Partido> getClassDeT() {
		return Partido.class;
	}

	@Override
	public String getNombreTabla() {
		return "PARTIDO";
	}
}