package mascota.repository;

import java.io.Serializable;

import mascota.dominio.Jugador;

public interface JugadorRepository extends IRepository<Long, Jugador>, Serializable{
}