package mascota.repository;

import java.io.Serializable;

import mascota.dominio.Equipo;

public interface EquipoRepository extends IRepository<Long, Equipo>, Serializable{
}