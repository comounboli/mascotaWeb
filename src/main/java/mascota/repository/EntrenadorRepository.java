package mascota.repository;

import java.io.Serializable;

import mascota.dominio.Entrenador;

public interface EntrenadorRepository extends IRepository<Long, Entrenador>, Serializable{
}